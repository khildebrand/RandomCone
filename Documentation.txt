Author: Kevin Hildebrand
Email: khildebrand@uchicago.edu

First time setup. Run commands (From within first RandomCone directory):

rcSetup Base,2.4.17
rc find_packages
rc compile

Each time after this when you log in from a new terminal all that is required is the command "rcSetup". You also need to setup Rucio,your grid proxy and panada. (localSetupRucio,voms-proxy-init -voms atlas, localSetupPanda)

From within the first RandomCone directory (poor naming convention).The second Random Cone directory just contains the code that takes an xAOD and produces a tree. This tree is what is used as input to perform the Random Cone study on.
The run directory is what contains all the scripts for doing the Random Cones and producing the tree from the xAOD.

Quick Overview with what needs to be run and in what order (I apoligize for the confusing naming convention, the code evolved over time however the name of the scripts did not.)

1) RandomCone package must be run over an xAOD to produce a tree.
2) makePlotsRanCone4.cxx is then ran over this tree and the A distribution (A=the difference in pT between two random cones) histograms are produced and saved in another root file.
3) SavePlots2.cxx can then be run to take the histograms from step 2) and produce histograms showing the results.

More details on each script:

1) RandomCone package.
	This package may be ran locally however it is adviced to run it on the grid (By default it runs on the grid).
	
	It is ran by using the script runsteerTreeMaker or runsteerTreeMakerData which call the scripts steerTreeMaker.cxx or steerTreeMakerData.cxx. The two scripts are almost identical, the only difference being which datasets they run on.
	They require two inputs. The first being where too write the output too and the second is what to append to the end of the filename. So for example to run this just going into the run directory and run the command:
	"./runsteerTreeMaker /share/t3data3/khildebr/RandomeConeOutputV1 v1"
	This will submit the job to the grid and once it is finished running you need to download the output. For the rest of this documentation lets assume it's been downloaded and one of the files you downloaded is called grid_output.root and
	is located in such a place thats its full path is /share/t3data3/khildebr/RandomConeOutputV1/someDirectory/grid_output.root.
	
	What is included in this grid_output.root file and how the tree is produced is controlled by a config file located in RandomCone/data. The default file used is called tree.config.
	A list of the available options are as follows:
	treeName (the name given to the tree in grid_output.root)
	JetContainerName (Name of the Jet Container you want to use in grid_output.root, can be left blank so you don't add any jet branch in the tree) 
	JetContainerName2 (Name of the Jet Container you want to use in grid_output.root, can be left blank so you don't add any jet branch in the tree)
	JetContainerName3 (Name of the Jet Container you want to use in grid_output.root, can be left blank so you don't add any jet branch in the tree)
	CaloClusterContainerName (Name of the Cluster Container you want to use in grid_output.root, can be left blank so you don't add any jets in the tree, however this is want you want to create random Cones from)
	PrimaryVertexContainerName (Name of the Primary Vertex Container you want to use in grid_output.root, can be left blank so you don't add any Primary Vertex branch in the tree)
	DoPileupReweighting (True/False, whether or not to use pileup reweighting tool)
	LumiCalcFiles (Path to the LumiCalcFiles, needed if you are doing pileup reweighting)
	PRWFiles (Path to the PRW Files, needed if you are doing pileup reweighting)
	ApplyGRLCut (True/False, whether or not to use GRL File and cut on GRL)
	GRL (Path to the GRL Fils, needed if you are cutting on GRL)
	DataScaleFactor (See Pileup reweighting tool for more info)
	histogramName/CustomPileupRebinning/doCustomPileupReweighting (these should be ignored and you should have doCustomPileupReweighting= False)
	There are other options I may have missed but this should be the important ones.
	
2) makePlotsRanCone4.cxx
	This script takes the grid_output.root file and generates Random Cones and produces the A distributions in 3-d histograms giving you A in blocks of eta as function of mu and NPV.
	
	This script was ran in batch on a condor system using the CondorSumbitMakePlots4.submit and CondorTestMakePlots4.sh scripts. It can also be ran locally though using runMakeRanConePlots4test.sh.
	makePlotsRanCone4.cxx has two inputs, the first is the input root file (eg "/share/t3data3/khildebr/RandomConeOutputV1/someDirectory/grid_output.root"), the second is the name of the output root file you want saved 
	(ex "output_makePlotsRanCone4.root"). These can be changed in the runMakeRanConePlots4test.sh script. 
	
	makePlotsRanCone4.cxx is controlled by the config script makePlotsRanCone4.config which must be located in the same directory as makePlotsRanCone4.cxx
	A list of the available options are as follows:
	isMC (True/False if the input xAOD was MC or data. If this is set too true it will use the event weight and pileup weight branches in the tree for weights)
	muScale(True/False if True it will scale all mu values by 1/1.16)
	newNames (True/False, This should always be True, this option is obsolete. False was necessary for old code)
	doSecondary (True/False, whether or not to do a second set of eta blocks)
	maxNPV (The max value of NPV)
	minNPV (The min value of NPV)
	maxMu  (The max Value of mu)
	minMu  (The min Value of mu)
	etaBlocks ( in what blocks of eta the random cones should be produced)
	etaBlocksSecondary  ( in what blocks of eta the random cones should be produced,only relevant if doSecondary=True)
	nBinsForA (the number of bins that should be used for the A distributions)
	range (the range that should be used for the A distribtions. A is in MeV, passing it a value of 20000 means the histogram will have a range of -20000 to 20000 for the A distribution with <nBinsForA> bins)
	
	
	This script takes output from step 1 and generates random cones in bins of eta given by etaBlocks and etaBlocksSecondary. It then saves a 3-d histogram for each eta block which gives A as a function of mu and NPV.
	
3) SavePlots2.cxx
	This script takes the output from step 2) and calculates the width of various A distributions. It is ran by the script runSavePlots2.sh and controlled by a config file.
	SavePlots2.cxx has 1 input which is the location of the config script. THe results of SavePlots2.cxx are in GeV.
	
	A list of the available options in the config script are as follows:
	outPutDir (the location where the png files are to be saved to)
	doData	(True/False whether or not to do data)
	AddedFileData (full path to the root file for data)
	doData	(True/False whether or not to do MC)
	AddedFileMC (full path to the root file for MC)
	doSecondary (True/False whether or not to do secondary eta binning)
	ToyTestInputFile (name of the output root file that is used by the script to generate toys)
	pileUpOutput (name of root file where pileup histograms are saved)
	maxNPV (The max value of NPV)
	minNPV (The min value of NPV)
	maxMu  (The max Value of mu)
	minMu  (The min Value of mu)
	etaBlocks ( in what blocks of eta the random cones should be produced)
	etaBlocksSecondary  ( in what blocks of eta the random cones should be produced,only relevant if doSecondary=True)
	The Previous 6 options should match the values in the config script for step 2)
	startEtaStep (which eta block to start in for noise studies)
	nEtaStepsToTake (the number of eta steps from startEtaStep to take to include in noise studies)
	nEtaStepsToTakeSecondary (the number of eta steps from startEtaStep to take to include in noise studies for secondary eta blocks)
	debug (True/False)
	doMuComparisonsFromMakePlots4 (True/False)
	
There is code for 

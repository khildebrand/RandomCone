#include <RandomCone/Algorithm.h>

#include <RandomCone/treeMaker.h>
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
#pragma link C++ class RC::AlgorithmRegistry+;
#pragma link C++ class RC::Algorithm+;
#pragma link C++ class treeMaker+;
#endif

#include "RandomCone/Algorithm.h"

// ROOT includes
#include <TSystem.h>

// RCU include for throwing an exception+message
#include <RootCoreUtils/ThrowMsg.h>



// this is needed to distribute the algorithm to the workers
ClassImp(RC::AlgorithmRegistry)

int RC::AlgorithmRegistry::countRegistered(std::string className){

  auto iter = m_registered_algos.find(className);

  if ( iter != m_registered_algos.end() ) {
    Info("countRegistered()","input class name: %s is already in the registry! Increase counter by 1 and return it", className.c_str() );
    m_registered_algos.at(className)++;
    return m_registered_algos.at(className);
  }

  Info("countRegistered()","input class name: %s is not registered yet. Returning 0", className.c_str() );

  return 0;

}

// this is needed to distribute the algorithm to the workers
ClassImp(RC::Algorithm)

RC::Algorithm::Algorithm() :
  m_name(""),
  m_classname(""),
  m_debug(false),
  m_verbose(false),
  m_systName(""),
  m_systVal(0),
  m_eventInfoContainerName("EventInfo"),
  m_configName(""),
  m_event(nullptr),
  m_store(nullptr),
  m_count_used(0)
{}

RC::Algorithm* RC::Algorithm::setName(std::string name){
  m_name = name;
  // call the TNamed
  this->SetName(name.c_str());
  return this;
}

RC::Algorithm* RC::Algorithm::setConfig(std::string configName){
  m_configName = configName;
  if ( !m_configName.empty() ) {
    /* check if file exists
     https://root.cern.ch/root/roottalk/roottalk02/5332.html */
    FileStat_t fStats;
    int fSuccess = gSystem->GetPathInfo(RC::Algorithm::getConfig(true).c_str(), fStats);
    if(fSuccess != 0){
      // could not find
      delete this;
      RCU_THROW_MSG("xAH::Algorithm::setConfig could not find the config file specified:\r\n\t\tConfig File: "+configName);
    }
  }
  return this;
}

std::string RC::Algorithm::getConfig(bool expand){
  if(expand) return gSystem->ExpandPathName( m_configName.c_str() );
  return m_configName;
}

RC::Algorithm* RC::Algorithm::setLevel(int level){
  m_debug = level & 1;
  m_verbose = (level >> 1)&1;
  return this;
}

RC::Algorithm* RC::Algorithm::setSyst(std::string systName){
  return this->setSyst(systName, 0);
}

RC::Algorithm* RC::Algorithm::setSyst(std::string systName, float systVal){
  m_systName = systName;
  m_systVal = systVal;
  return this;
}

RC::Algorithm* RC::Algorithm::setSyst(std::string systName, std::vector<float> systValVector){
  m_systName = systName;
  m_systValVector = systValVector;
  return this;
}


RC::Algorithm* RC::Algorithm::registerClass(RC::AlgorithmRegistry &reg, std::string className){

  Info("registerClass()","input class name: %s", className.c_str() );

  m_classname = className;

  // the function will return 0 if the algo
  // isn't in the registry yet
  m_count_used = reg.countRegistered(className);

  // if not found already, set in the map in the registry the name of the algo
  // and assign a value of 0 to the counter
  if ( m_count_used == 0 ) { reg.m_registered_algos[className] = m_count_used; }

  return this;

}


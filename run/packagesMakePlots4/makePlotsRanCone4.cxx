//#include "TMath.h"
//#include "TRandom1.h"

int counterBad =0;
int counterGood =0;
void ATLASLabel(Double_t x,Double_t y,const char* text,double tsize=.08,Color_t color = 1);
void ATLASLabel(Double_t x,Double_t y,const char* text,double tsize,Color_t color)
{
  TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize);
  l.SetTextSize(tsize);
  l.SetNDC();
  l.SetTextFont(72);
  l.SetTextColor(color);

  double delx = 0.115*696*gPad->GetWh()/(472*gPad->GetWw());

  l.DrawLatex(x,y,"ATLAS");
  if (text) {
    TLatex p;
    p.SetTextSize(tsize);
    p.SetNDC();
    p.SetTextFont(42);
    p.SetTextColor(color);
    p.DrawLatex(x+delx,y,text);
  }
}
void ATLASLabel2(Double_t x,Double_t y,const char* text,double tsize=.08,Color_t color = 1);
void ATLASLabel2(Double_t x,Double_t y,const char* text,double tsize,Color_t color)
{
  TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize);
  l.SetTextSize(tsize);
  l.SetNDC();
  l.SetTextFont(72);
  l.SetTextColor(color);

  double delx = .7*0.115*696*gPad->GetWh()/(472*gPad->GetWw());

  l.DrawLatex(x,y,"ATLAS");
  if (text) {
    TLatex p;
    p.SetTextSize(tsize);
    p.SetNDC();
    p.SetTextFont(42);
    p.SetTextColor(color);
    p.DrawLatex(x+delx,y,text);
  }
}
void ATLASLabel3(Double_t x,Double_t y,const char* text,double labelOffset=1.,double tsize=.08,Color_t color = 1);
void ATLASLabel3(Double_t x,Double_t y,const char* text,double labelOffset,double tsize,Color_t color)
{
  TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize);
  l.SetTextSize(tsize);
  l.SetNDC();
  l.SetTextFont(72);
  l.SetTextColor(color);

  double delx = labelOffset*0.115*696*gPad->GetWh()/(472*gPad->GetWw());

  l.DrawLatex(x,y,"ATLAS");
  if (text) {
    TLatex p;
    p.SetTextSize(tsize);
    p.SetNDC();
    p.SetTextFont(42);
    p.SetTextColor(color);
    p.DrawLatex(x+delx,y,text);
  }
}
Double_t calculateDifferenceOldBroken(vector<float>* tCaloCluster_pt,vector<float>* tCaloCluster_phi,vector<float>* tCaloCluster_eta,Double_t lowerEta,Double_t upperEta,Double_t RCone,bool extraHist,TH1D* histoEta1,TH1D* histoEta2,TH1D* histoPhi1,TH1D* histoPhi2);
Double_t calculateDifferenceOldBroken(vector<float>* tCaloCluster_pt,vector<float>* tCaloCluster_phi,vector<float>* tCaloCluster_eta,Double_t lowerEta,Double_t upperEta,Double_t RCone,bool extraHist=false,TH1D* histoEta1=NULL,TH1D* histoEta2=NULL,TH1D* histoPhi1=NULL,TH1D* histoPhi2=NULL) {
	TRandom *r1=new TRandom1();
	Double_t randomNumber=r1->Rndm();
	Double_t rangePhi=TMath::Pi()*2.;
	Double_t phi1=-TMath::Pi()+randomNumber*rangePhi;
	Double_t phi2;
	if (phi1>0) {
		phi2= phi1-TMath::Pi();
	} else {
		phi2=phi1+TMath::Pi();
	}

	//cout << "phi1="<< phi1 << " phi2="<<phi2<<endl;
	randomNumber=r1->Rndm();
	Double_t rangeEta=upperEta-lowerEta;
	Double_t eta1=lowerEta+randomNumber*rangeEta;
	randomNumber=r1->Rndm();
	Double_t eta2=lowerEta+randomNumber*rangeEta;

	if (extraHist==true) {
		histoPhi1->Fill(phi1);
		histoPhi2->Fill(phi2);
		histoEta1->Fill(eta1);
		histoEta2->Fill(eta2);

	}
	//cout << "eta1="<< eta1 << " eta2="<<eta2<<endl;
	Double_t pt1=0;
	Double_t pt2=0;
	bool flagSomethingInACone=false;
	for (int i=0;i<tCaloCluster_pt->size();i++) {
		if (TMath::Sqrt((tCaloCluster_phi->at(i)-phi1)*(tCaloCluster_phi->at(i)-phi1)+(tCaloCluster_eta->at(i)-eta1)*(tCaloCluster_eta->at(i)-eta1))<RCone) {
			pt1+=tCaloCluster_pt->at(i);
			flagSomethingInACone=true;
		}
		if (TMath::Sqrt((tCaloCluster_phi->at(i)-phi2)*(tCaloCluster_phi->at(i)-phi2)+(tCaloCluster_eta->at(i)-eta2)*(tCaloCluster_eta->at(i)-eta2))<RCone) {
			pt2+=tCaloCluster_pt->at(i);
			flagSomethingInACone=true;
		}
	}
	//if (flagSomethingInACone==true)  {
		return (pt1-pt2);
	//}
	//return 3000000;
}
Double_t calculateDifferencePointers(vector<float>* tCaloCluster_pt,vector<float>* tCaloCluster_e,vector<float>* tCaloCluster_m,vector<float>* tCaloCluster_phi,vector<float>* tCaloCluster_eta,Double_t lowerEta,Double_t upperEta,Double_t RCone,bool extraHist,TH1D* histoEta1,TH1D* histoEta2,TH1D* histoPhi1,TH1D* histoPhi2);
Double_t calculateDifferencePointers(vector<float>* tCaloCluster_pt,vector<float>* tCaloCluster_e,vector<float>* tCaloCluster_m,vector<float>* tCaloCluster_phi,vector<float>* tCaloCluster_eta,Double_t lowerEta,Double_t upperEta,Double_t RCone,bool extraHist=false,TH1D* histoEta1=NULL,TH1D* histoEta2=NULL,TH1D* histoPhi1=NULL,TH1D* histoPhi2=NULL) {
	TRandom *r1=new TRandom1();
	Double_t randomNumber=r1->Rndm();
	Double_t rangePhi=TMath::Pi()*2.;
	Double_t phi1=-TMath::Pi()+randomNumber*rangePhi;
	Double_t phi2;
	if (phi1>0) {
		phi2= phi1-TMath::Pi();
	} else {
		phi2=phi1+TMath::Pi();
	}

	//cout << "phi1="<< phi1 << " phi2="<<phi2<<endl;
	randomNumber=r1->Rndm();
	Double_t rangeEta=upperEta-lowerEta;
	Double_t eta1=lowerEta+randomNumber*rangeEta;
	randomNumber=r1->Rndm();
	Double_t eta2=lowerEta+randomNumber*rangeEta;

	TLorentzVector *rndmCone1;
	TLorentzVector *rndmCone1b;
	TLorentzVector *rndmCone2;

	rndmCone1->SetPtEtaPhiE(300,eta1,phi1,100);
	rndmCone1b->SetPtEtaPhiE(4000.,eta1,phi1,3000.);
	rndmCone2->SetPtEtaPhiE(1000,eta2,phi2,3000);
	cout <<"rndmCone1: "<< "PseudoRapidity=" << 	rndmCone1->PseudoRapidity()<< " Rapidity="<<rndmCone1->Rapidity() << " Phi=" <<rndmCone1->Phi()<<" Px="<<rndmCone1->Px()<<" Py="<<rndmCone1->Py()<<" Pz="<<rndmCone1->Pz()<<" E="<<rndmCone1->E()<<" M="<<rndmCone1->M()<<endl;
	cout <<"rndmCone1b: "<< "PseudoRapidity=" << 	rndmCone1b->PseudoRapidity()<< " Rapidity="<<rndmCone1b->Rapidity() << " Phi=" <<rndmCone1b->Phi()<<" Px="<<rndmCone1b->Px()<<" Py="<<rndmCone1b->Py()<<" Pz="<<rndmCone1b->Pz()<<" E="<<rndmCone1b->E()<<" M="<<rndmCone1b->M()<<endl;
	cout <<"rndmCone2: "<< "PseudoRapidity=" << 	rndmCone2->PseudoRapidity()<< " Rapidity="<<rndmCone2->Rapidity() << " Phi=" <<rndmCone2->Phi()<<" Px="<<rndmCone2->Px()<<" Py="<<rndmCone2->Py()<<" Pz="<<rndmCone2->Pz()<<" E="<<rndmCone2->E()<<" M="<<rndmCone2->M()<<endl;

	if (extraHist==true) {
		histoPhi1->Fill(phi1);
		histoPhi2->Fill(phi2);
		histoEta1->Fill(eta1);
		histoEta2->Fill(eta2);

	}
	//cout << "eta1="<< eta1 << " eta2="<<eta2<<endl;
	Double_t pt1=0;
	Double_t pt2=0;
	TLorentzVector *clusterVectorE;
	TLorentzVector *clusterVectorM;
	bool flagSomethingInACone=false;

	for (int i=0;i<tCaloCluster_pt->size();i++) {
		clusterVectorE->SetPtEtaPhiE(tCaloCluster_pt->at(i),tCaloCluster_eta->at(i),tCaloCluster_phi->at(i),tCaloCluster_e->at(i));
		cout <<"VectorE: "<< "PseudoRapidity=" << 	clusterVectorE->PseudoRapidity()<< " Rapidity="<<clusterVectorE->Rapidity() << " Phi=" <<clusterVectorE->Phi()<<" Px="<<clusterVectorE->Px()<<" Py="<<clusterVectorE->Py()<<" Pz="<<clusterVectorE->Pz()<<" E="<<clusterVectorE->E()<<" M="<<clusterVectorE->M()<<endl;

		//cout <<tCaloCluster_pt->at(i)<<" "<<tCaloCluster_eta->at(i)<<" "<<tCaloCluster_phi->at(i)<<" "<<tCaloCluster_m->at(i)<<" "<<tCaloCluster_e->at(i)<<endl;
		//clusterVectorM->SetPtEtaPhiM(tCaloCluster_pt->at(i),tCaloCluster_eta->at(i),tCaloCluster_phi->at(i),tCaloCluster_m->at(i));
		//cout <<"VectorM: "<< "PseudoRapidity=" << 	clusterVectorM->PseudoRapidity()<< " Rapidity="<<clusterVectorM->Rapidity() << " Phi=" <<clusterVectorM->Phi()<<" Px="<<clusterVectorM->Px()<<" Py="<<clusterVectorM->Py()<<" Pz="<<clusterVectorM->Pz()<<" E="<<clusterVectorM->E()<<" M="<<clusterVectorM->M()<<endl;

		cout <<"DeltaR1="<< rndmCone1->DeltaR(*clusterVectorE) <<endl;
		cout <<"DeltaR1b="<< rndmCone1b->DeltaR(*clusterVectorE) <<endl;
		if (rndmCone1->DeltaR(*clusterVectorE)<RCone) {
			pt1+=tCaloCluster_pt->at(i);
			flagSomethingInACone=true;
		}
		cout <<"DeltaR2="<< rndmCone2->DeltaR(*clusterVectorE) <<endl;
		if (rndmCone2->DeltaR(*clusterVectorE)<RCone) {
			pt2+=tCaloCluster_pt->at(i);
			flagSomethingInACone=true;
		}
	}
	//if (flagSomethingInACone==true)  {
		return (pt1-pt2);
	//}
	//return 3000000;
}
Double_t calculateDifference(vector<float>* tCaloCluster_pt,vector<float>* tCaloCluster_e,vector<float>* tCaloCluster_m,vector<float>* tCaloCluster_phi,vector<float>* tCaloCluster_eta,Double_t lowerEta,Double_t upperEta,Double_t RCone,bool extraHist,TH1D* histoEta1,TH1D* histoEta2,TH1D* histoPhi1,TH1D* histoPhi2);
Double_t calculateDifference(vector<float>* tCaloCluster_pt,vector<float>* tCaloCluster_e,vector<float>* tCaloCluster_m,vector<float>* tCaloCluster_phi,vector<float>* tCaloCluster_eta,Double_t lowerEta,Double_t upperEta,Double_t RCone,bool extraHist=false,TH1D* histoEta1=NULL,TH1D* histoEta2=NULL,TH1D* histoPhi1=NULL,TH1D* histoPhi2=NULL) {
	TRandom *r1=new TRandom1();
	r1->SetSeed(0);
	Double_t randomNumber=r1->Rndm();
	Double_t rangePhi=TMath::Pi()*2.;
	Double_t phi1=-TMath::Pi()+randomNumber*rangePhi;
	Double_t phi2;
	if (phi1>0) {
		phi2= phi1-TMath::Pi();
	} else {
		phi2=phi1+TMath::Pi();
	}

	//cout << "phi1="<< phi1 << " phi2="<<phi2<<endl;
	randomNumber=r1->Rndm();
	Double_t rangeEta=upperEta-lowerEta;
	Double_t eta1=lowerEta+randomNumber*rangeEta;
	randomNumber=r1->Rndm();
	Double_t eta2=lowerEta+randomNumber*rangeEta;

	TLorentzVector rndmCone1;
	TLorentzVector rndmCone1b;
	TLorentzVector rndmCone2;

	rndmCone1.SetPtEtaPhiE(300,eta1,phi1,100);
	//rndmCone1b.SetPtEtaPhiE(4000.,eta1,phi1,3000.);
	rndmCone2.SetPtEtaPhiE(1000,eta2,phi2,3000);
	//cout <<"rndmCone1: "<< "PseudoRapidity=" << 	rndmCone1.PseudoRapidity()<< " Rapidity="<<rndmCone1.Rapidity() << " Phi=" <<rndmCone1.Phi()<<" Px="<<rndmCone1.Px()<<" Py="<<rndmCone1.Py()<<" Pz="<<rndmCone1.Pz()<<" E="<<rndmCone1.E()<<" M="<<rndmCone1.M()<<endl;
	//cout <<"rndmCone1b: "<< "PseudoRapidity=" << 	rndmCone1b.PseudoRapidity()<< " Rapidity="<<rndmCone1b.Rapidity() << " Phi=" <<rndmCone1b.Phi()<<" Px="<<rndmCone1b.Px()<<" Py="<<rndmCone1b.Py()<<" Pz="<<rndmCone1b.Pz()<<" E="<<rndmCone1b.E()<<" M="<<rndmCone1b.M()<<endl;
	//cout <<"rndmCone2: "<< "PseudoRapidity=" << 	rndmCone2.PseudoRapidity()<< " Rapidity="<<rndmCone2.Rapidity() << " Phi=" <<rndmCone2.Phi()<<" Px="<<rndmCone2.Px()<<" Py="<<rndmCone2.Py()<<" Pz="<<rndmCone2.Pz()<<" E="<<rndmCone2.E()<<" M="<<rndmCone2.M()<<endl;

	if (extraHist==true) {
		histoPhi1->Fill(phi1);
		histoPhi2->Fill(phi2);
		histoEta1->Fill(eta1);
		histoEta2->Fill(eta2);

	}
	//cout << "eta1="<< eta1 << " eta2="<<eta2<<endl;
	Double_t pt1=0;
	Double_t pt2=0;
	TLorentzVector clusterVectorE;
	TLorentzVector clusterVectorM;
	bool flagSomethingInACone=false;

	for (int i=0;i<tCaloCluster_pt->size();i++) {
		clusterVectorE.SetPtEtaPhiE(tCaloCluster_pt->at(i),tCaloCluster_eta->at(i),tCaloCluster_phi->at(i),tCaloCluster_e->at(i));
		//cout <<"VectorE: "<< "PseudoRapidity=" << 	clusterVectorE.PseudoRapidity()<< " Rapidity="<<clusterVectorE.Rapidity() << " Phi=" <<clusterVectorE.Phi()<<" Px="<<clusterVectorE.Px()<<" Py="<<clusterVectorE.Py()<<" Pz="<<clusterVectorE.Pz()<<" E="<<clusterVectorE.E()<<" M="<<clusterVectorE.M()<<endl;

		//cout <<tCaloCluster_pt->at(i)<<" "<<tCaloCluster_eta->at(i)<<" "<<tCaloCluster_phi->at(i)<<" "<<tCaloCluster_m->at(i)<<" "<<tCaloCluster_e->at(i)<<endl;
		//clusterVectorM->SetPtEtaPhiM(tCaloCluster_pt->at(i),tCaloCluster_eta->at(i),tCaloCluster_phi->at(i),tCaloCluster_m->at(i));
		//cout <<"VectorM: "<< "PseudoRapidity=" << 	clusterVectorM->PseudoRapidity()<< " Rapidity="<<clusterVectorM->Rapidity() << " Phi=" <<clusterVectorM->Phi()<<" Px="<<clusterVectorM->Px()<<" Py="<<clusterVectorM->Py()<<" Pz="<<clusterVectorM->Pz()<<" E="<<clusterVectorM->E()<<" M="<<clusterVectorM->M()<<endl;

		//cout <<"DeltaR1="<< rndmCone1.DeltaR(clusterVectorE) <<endl;
		//cout <<"DeltaR1b="<< rndmCone1b.DeltaR(clusterVectorE) <<endl;
		if (TMath::Abs(rndmCone1.DeltaR(clusterVectorE))<RCone) {
			pt1+=tCaloCluster_pt->at(i);
			flagSomethingInACone=true;
		}
		//cout <<"DeltaR2="<< rndmCone2.DeltaR(clusterVectorE) <<endl;
		if (TMath::Abs(rndmCone2.DeltaR(clusterVectorE))<RCone) {
			pt2+=tCaloCluster_pt->at(i);
			flagSomethingInACone=true;
		}
	}
	if (flagSomethingInACone==true)  {
		return (pt1-pt2);
	} else {
		cout <<"rndmCone1: "<< "PseudoRapidity=" << 	rndmCone1.PseudoRapidity()<< " Rapidity="<<rndmCone1.Rapidity() << " Phi=" <<rndmCone1.Phi()<<" Px="<<rndmCone1.Px()<<" Py="<<rndmCone1.Py()<<" Pz="<<rndmCone1.Pz()<<" E="<<rndmCone1.E()<<" M="<<rndmCone1.M()<<endl;
		cout <<"rndmCone2: "<< "PseudoRapidity=" << 	rndmCone2.PseudoRapidity()<< " Rapidity="<<rndmCone2.Rapidity() << " Phi=" <<rndmCone2.Phi()<<" Px="<<rndmCone2.Px()<<" Py="<<rndmCone2.Py()<<" Pz="<<rndmCone2.Pz()<<" E="<<rndmCone2.E()<<" M="<<rndmCone2.M()<<endl;
		cout <<"VectorE: "<< "PseudoRapidity=" << 	clusterVectorE.PseudoRapidity()<< " Rapidity="<<clusterVectorE.Rapidity() << " Phi=" <<clusterVectorE.Phi()<<" Px="<<clusterVectorE.Px()<<" Py="<<clusterVectorE.Py()<<" Pz="<<clusterVectorE.Pz()<<" E="<<clusterVectorE.E()<<" M="<<clusterVectorE.M()<<endl;

		cout <<"DeltaR1="<< rndmCone1.DeltaR(clusterVectorE) <<endl;
		cout <<"DeltaR2="<< rndmCone2.DeltaR(clusterVectorE) <<endl;
		return (pt1-pt2);
	}
	//return 3000000;
}
Double_t calculateDifferenceStats(vector<float>* tCaloCluster_pt,vector<float>* tCaloCluster_e,vector<float>* tCaloCluster_m,vector<float>* tCaloCluster_phi,vector<float>* tCaloCluster_eta,Double_t lowerEta,Double_t upperEta,Double_t RCone,bool extraHist,TH1D* histoEta1,TH1D* histoEta2,TH1D* histoPhi1,TH1D* histoPhi2,TH1D* histoNearestConeMiss,TH1D* histoNearestConeHit,TH1D* histoNPVHit,TH1D* histoNPVMiss,int tNPV_n,TH2F* histoEtaPhiHit,TH2F* histoEtaPhiMiss,TH2F* histoEtaPhiConeHit,TH2F* histoEtaPhiConeMiss,TH2F* R4ConeEnergyEtaHit,TH2F* R4ConeEnergyPhiHit,TH2F* histoEtaPhiHitEnergy,TH2F* histoEnergyNVP,TH1D* histoFractionAtLeast1Pos,TH1D* histoFractionAtLeast1PosFcnNumberPosClusters,TH1D* histoFractionAtLeast1PosFcnNumberNegClusters,TH1D* histoFractionAtLeast1PosFcnClusterPtInCone,TH1D* histoFractionConesAtLeast1Pos,TH1D* histoFractionConesAtLeast1PosFcnNumberPosClusters,TH1D* histoFractionConesAtLeast1PosFcnNumberNegClusters,TH1D* histoFractionConesAtLeast1PosFcnClusterPtInCone);
Double_t calculateDifferenceStats(vector<float>* tCaloCluster_pt,vector<float>* tCaloCluster_e,vector<float>* tCaloCluster_m,vector<float>* tCaloCluster_phi,vector<float>* tCaloCluster_eta,Double_t lowerEta,Double_t upperEta,Double_t RCone,bool extraHist=false,TH1D* histoEta1=NULL,TH1D* histoEta2=NULL,TH1D* histoPhi1=NULL,TH1D* histoPhi2=NULL,TH1D* histoNearestConeMiss=NULL,TH1D* histoNearestConeHit=NULL,TH1D* histoNPVHit=NULL,TH1D* histoNPVMiss=NULL,int tNPV_n=-1,TH2F* histoEtaPhiHit=NULL,TH2F* histoEtaPhiMiss=NULL,TH2F* histoEtaPhiConeHit=NULL,TH2F* histoEtaPhiConeMiss=NULL,TH2F* R4ConeEnergyEtaHit=NULL,TH2F* R4ConeEnergyPhiHit=NULL,TH2F* histoEtaPhiHitEnergy=NULL,TH2F* histoEnergyNVP=NULL,TH1D* histoFractionAtLeast1Pos=NULL,TH1D* histoFractionAtLeast1PosFcnNumberPosClusters=NULL,TH1D* histoFractionAtLeast1PosFcnNumberNegClusters=NULL,TH1D* histoFractionAtLeast1PosFcnClusterPtInCone=NULL,TH1D* histoFractionConesAtLeast1Pos=NULL,TH1D* histoFractionConesAtLeast1PosFcnNumberPosClusters=NULL,TH1D* histoFractionConesAtLeast1PosFcnNumberNegClusters=NULL,TH1D* histoFractionConesAtLeast1PosFcnClusterPtInCone=NULL) {
	//histoFractionAtLeast1Pos,histoFractionAtLeast1PosFcnNumberPosClusters,histoFractionAtLeast1PosFcnNumberNegClusters,histoFractionAtLeast1PosFcnClusterPtInCone
	TRandom *r1=new TRandom1();
	r1->SetSeed(0);
	bool flagSomethingInCone1=false;
	bool flagSomethingInCone2=false;

	Double_t pt1=0;
	Double_t pt2=0;
	TLorentzVector clusterVectorE;
	TLorentzVector clusterVectorM;
	bool flagSomethingInACone=false;
	bool flagSomethingInAConePos=false;
	bool flagAtLeastOneConeEmpty=true;
	bool flagSomethingConeNoPosPt=true;
	Double_t smallestDistance=100;

	bool flagSomethingInConePos1=false;
	bool flagSomethingInConePos2=false;

	int counterNumberOfPosPtClustersInEvent=0;
	int counterNumberOfNegPtClustersInEvent=0;

	TLorentzVector rndmCone1;
	TLorentzVector rndmCone1b;
	TLorentzVector rndmCone2;

	Double_t eta1;
	Double_t eta2;
	Double_t phi1;
	Double_t phi2;
	int numberOfTries=0;
	while ((flagSomethingInCone1==false||flagSomethingInCone2==false)&&numberOfTries<50) {
		numberOfTries++;
		Double_t randomNumber=r1->Rndm();
		Double_t rangePhi=TMath::Pi()*2.;
		Double_t phi1=-TMath::Pi()+randomNumber*rangePhi;
		Double_t phi2;
		if (phi1>0) {
			phi2= phi1-TMath::Pi();
		} else {
			phi2=phi1+TMath::Pi();
		}

		//cout << "phi1="<< phi1 << " phi2="<<phi2<<endl;
		randomNumber=r1->Rndm();
		Double_t rangeEta=upperEta-lowerEta;
		Double_t eta1=lowerEta+randomNumber*rangeEta;
		randomNumber=r1->Rndm();
		Double_t eta2=lowerEta+randomNumber*rangeEta;

		rndmCone1;
		rndmCone1b;
		rndmCone2;

		rndmCone1.SetPtEtaPhiE(300,eta1,phi1,100);
		//rndmCone1b.SetPtEtaPhiE(4000.,eta1,phi1,3000.);
		rndmCone2.SetPtEtaPhiE(1000,eta2,phi2,3000);
		//cout <<"rndmCone1: "<< "PseudoRapidity=" << 	rndmCone1.PseudoRapidity()<< " Rapidity="<<rndmCone1.Rapidity() << " Phi=" <<rndmCone1.Phi()<<" Px="<<rndmCone1.Px()<<" Py="<<rndmCone1.Py()<<" Pz="<<rndmCone1.Pz()<<" E="<<rndmCone1.E()<<" M="<<rndmCone1.M()<<endl;
		//cout <<"rndmCone1b: "<< "PseudoRapidity=" << 	rndmCone1b.PseudoRapidity()<< " Rapidity="<<rndmCone1b.Rapidity() << " Phi=" <<rndmCone1b.Phi()<<" Px="<<rndmCone1b.Px()<<" Py="<<rndmCone1b.Py()<<" Pz="<<rndmCone1b.Pz()<<" E="<<rndmCone1b.E()<<" M="<<rndmCone1b.M()<<endl;
		//cout <<"rndmCone2: "<< "PseudoRapidity=" << 	rndmCone2.PseudoRapidity()<< " Rapidity="<<rndmCone2.Rapidity() << " Phi=" <<rndmCone2.Phi()<<" Px="<<rndmCone2.Px()<<" Py="<<rndmCone2.Py()<<" Pz="<<rndmCone2.Pz()<<" E="<<rndmCone2.E()<<" M="<<rndmCone2.M()<<endl;

		if (extraHist==true) {
			histoPhi1->Fill(phi1);
			histoPhi2->Fill(phi2);
			histoEta1->Fill(eta1);
			histoEta2->Fill(eta2);

		}
		histoEtaPhiConeHit->Fill(phi1,eta1);
		histoEtaPhiConeMiss->Fill(phi2,eta2);
		//cout << "eta1="<< eta1 << " eta2="<<eta2<<endl;
		pt1=0;
		pt2=0;
		//TLorentzVector clusterVectorE;
		//TLorentzVector clusterVectorM;
		flagSomethingInACone=false;
		flagSomethingInAConePos=false;
		flagAtLeastOneConeEmpty=true;
		flagSomethingConeNoPosPt=true;
		smallestDistance=100;

		flagSomethingInConePos1=false;
		flagSomethingInConePos2=false;

		counterNumberOfPosPtClustersInEvent=0;
		counterNumberOfNegPtClustersInEvent=0;
		for (int i=0;i<tCaloCluster_pt->size();i++) {
			clusterVectorE.SetPtEtaPhiE(tCaloCluster_pt->at(i),tCaloCluster_eta->at(i),tCaloCluster_phi->at(i),tCaloCluster_e->at(i));
			if (tCaloCluster_pt->at(i)>0) {
				counterNumberOfPosPtClustersInEvent++;
			}
			if (tCaloCluster_pt->at(i)<0) {
				counterNumberOfNegPtClustersInEvent++;
			}
			//cout <<"VectorE: "<< "PseudoRapidity=" << 	clusterVectorE.PseudoRapidity()<< " Rapidity="<<clusterVectorE.Rapidity() << " Phi=" <<clusterVectorE.Phi()<<" Px="<<clusterVectorE.Px()<<" Py="<<clusterVectorE.Py()<<" Pz="<<clusterVectorE.Pz()<<" E="<<clusterVectorE.E()<<" M="<<clusterVectorE.M()<<endl;

			//cout <<tCaloCluster_pt->at(i)<<" "<<tCaloCluster_eta->at(i)<<" "<<tCaloCluster_phi->at(i)<<" "<<tCaloCluster_m->at(i)<<" "<<tCaloCluster_e->at(i)<<endl;
			//clusterVectorM->SetPtEtaPhiM(tCaloCluster_pt->at(i),tCaloCluster_eta->at(i),tCaloCluster_phi->at(i),tCaloCluster_m->at(i));
			//cout <<"VectorM: "<< "PseudoRapidity=" << 	clusterVectorM->PseudoRapidity()<< " Rapidity="<<clusterVectorM->Rapidity() << " Phi=" <<clusterVectorM->Phi()<<" Px="<<clusterVectorM->Px()<<" Py="<<clusterVectorM->Py()<<" Pz="<<clusterVectorM->Pz()<<" E="<<clusterVectorM->E()<<" M="<<clusterVectorM->M()<<endl;

			//cout <<"DeltaR1="<< rndmCone1.DeltaR(clusterVectorE) <<endl;
			//cout <<"DeltaR1b="<< rndmCone1b.DeltaR(clusterVectorE) <<endl;
			if (TMath::Abs(rndmCone1.DeltaR(clusterVectorE))<smallestDistance) {
				smallestDistance=TMath::Abs(rndmCone1.DeltaR(clusterVectorE));
			}
			if (TMath::Abs(rndmCone1.DeltaR(clusterVectorE))<RCone) {
				pt1+=tCaloCluster_pt->at(i);
				R4ConeEnergyEtaHit->Fill(tCaloCluster_pt->at(i),tCaloCluster_eta->at(i));
				R4ConeEnergyPhiHit->Fill(tCaloCluster_pt->at(i),tCaloCluster_phi->at(i));
				flagSomethingInACone=true;
				flagSomethingInCone1=true;
				if (tCaloCluster_pt->at(i)>0) {
					flagSomethingInAConePos=true;
				}
				if (tCaloCluster_pt->at(i)>0) {
					flagSomethingInConePos1=true;
				}
			}
			//cout <<"DeltaR2="<< rndmCone2.DeltaR(clusterVectorE) <<endl;
			if (TMath::Abs(rndmCone2.DeltaR(clusterVectorE))<RCone) {
				pt2+=tCaloCluster_pt->at(i);
				R4ConeEnergyEtaHit->Fill(tCaloCluster_pt->at(i),tCaloCluster_eta->at(i));
				R4ConeEnergyPhiHit->Fill(tCaloCluster_pt->at(i),tCaloCluster_phi->at(i));
				flagSomethingInACone=true;
				flagSomethingInCone2=true;
				if (tCaloCluster_pt->at(i)>0) {
					flagSomethingInAConePos=true;
				}
				if (tCaloCluster_pt->at(i)>0) {
					flagSomethingInConePos2=true;
				}
			}
			/*if (tCaloCluster_pt->at(i)<0) {
				cout<<"                    HELLLO"<<endl;
			}*/
		}
	}
	if (numberOfTries<=50) {

		if (flagSomethingInConePos1==false||flagSomethingInConePos2==false) {
			flagSomethingConeNoPosPt=true;
			histoFractionAtLeast1Pos->Fill(0);
			histoFractionAtLeast1PosFcnNumberPosClusters->Fill(counterNumberOfPosPtClustersInEvent);
			histoFractionAtLeast1PosFcnNumberNegClusters->Fill(counterNumberOfNegPtClustersInEvent);
			if (pt1>pt2) {
				histoFractionAtLeast1PosFcnClusterPtInCone->Fill(pt1);
			} else {
				histoFractionAtLeast1PosFcnClusterPtInCone->Fill(pt2);
			}

			histoFractionConesAtLeast1Pos->Fill(0);
			histoFractionConesAtLeast1PosFcnNumberPosClusters->Fill(counterNumberOfPosPtClustersInEvent);
			histoFractionConesAtLeast1PosFcnNumberNegClusters->Fill(counterNumberOfNegPtClustersInEvent);
			if (pt1>pt2) {
				histoFractionConesAtLeast1PosFcnClusterPtInCone->Fill(pt1);
			} else {
				histoFractionConesAtLeast1PosFcnClusterPtInCone->Fill(pt2);
			}
			if (flagSomethingInConePos1==false&&flagSomethingInConePos2==false) {
				histoFractionConesAtLeast1Pos->Fill(0);
				histoFractionConesAtLeast1PosFcnNumberPosClusters->Fill(counterNumberOfPosPtClustersInEvent);
				histoFractionConesAtLeast1PosFcnNumberNegClusters->Fill(counterNumberOfNegPtClustersInEvent);
				if (pt1>pt2) {
					histoFractionConesAtLeast1PosFcnClusterPtInCone->Fill(pt1);
				} else {
					histoFractionConesAtLeast1PosFcnClusterPtInCone->Fill(pt2);
				}
			}
		}
		if (flagSomethingInACone==true)  {
			histoNPVHit->Fill(tNPV_n);
			histoNearestConeHit->Fill(smallestDistance);
			bool flagSomethingInCone1=false;
			bool flagSomethingInCone2=false;


			for (int i=0;i<tCaloCluster_pt->size();i++) {

				if (TMath::Abs(rndmCone1.DeltaR(clusterVectorE))<RCone) {
					histoEtaPhiHit->Fill(tCaloCluster_phi->at(i),tCaloCluster_eta->at(i));
					histoEtaPhiHitEnergy->Fill(tCaloCluster_phi->at(i),tCaloCluster_eta->at(i),tCaloCluster_pt->at(i));
					flagSomethingInCone1=true;

				}
				//cout <<"DeltaR2="<< rndmCone2.DeltaR(clusterVectorE) <<endl;
				if (TMath::Abs(rndmCone2.DeltaR(clusterVectorE))<RCone) {
					histoEtaPhiHit->Fill(tCaloCluster_phi->at(i),tCaloCluster_eta->at(i));
					histoEtaPhiHitEnergy->Fill(tCaloCluster_phi->at(i),tCaloCluster_eta->at(i),tCaloCluster_pt->at(i));
					flagSomethingInCone2=true;

				}
			}

			if (flagSomethingInCone1==false||flagSomethingInCone2==false) {
				flagAtLeastOneConeEmpty=true;
			}
			if (counterGood<1) {
				cout <<"rndmCone1: "<< "PseudoRapidity=" << 	rndmCone1.PseudoRapidity()<< " Rapidity="<<rndmCone1.Rapidity() << " Phi=" <<rndmCone1.Phi()<<" Px="<<rndmCone1.Px()<<" Py="<<rndmCone1.Py()<<" Pz="<<rndmCone1.Pz()<<" E="<<rndmCone1.E()<<" M="<<rndmCone1.M()<<endl;
				cout <<"rndmCone2: "<< "PseudoRapidity=" << 	rndmCone2.PseudoRapidity()<< " Rapidity="<<rndmCone2.Rapidity() << " Phi=" <<rndmCone2.Phi()<<" Px="<<rndmCone2.Px()<<" Py="<<rndmCone2.Py()<<" Pz="<<rndmCone2.Pz()<<" E="<<rndmCone2.E()<<" M="<<rndmCone2.M()<<endl;
				cout <<"VectorE: "<< "PseudoRapidity=" << 	clusterVectorE.PseudoRapidity()<< " Rapidity="<<clusterVectorE.Rapidity() << " Phi=" <<clusterVectorE.Phi()<<" Px="<<clusterVectorE.Px()<<" Py="<<clusterVectorE.Py()<<" Pz="<<clusterVectorE.Pz()<<" E="<<clusterVectorE.E()<<" M="<<clusterVectorE.M()<<endl;

				cout <<"DeltaR1="<< rndmCone1.DeltaR(clusterVectorE) <<endl;
				cout <<"DeltaR2="<< rndmCone2.DeltaR(clusterVectorE) <<endl;

				TCanvas *c1 = new TCanvas("c1");
				TH2F *histoEtaPhi = new TH2F("h","h",100,-3.2,3.2,100,-.8,.8);
				for (int i=0;i<tCaloCluster_pt->size();i++) {

					clusterVectorE.SetPtEtaPhiE(tCaloCluster_pt->at(i),tCaloCluster_eta->at(i),tCaloCluster_phi->at(i),tCaloCluster_e->at(i));
					//cout <<"VectorE: "<< "PseudoRapidity=" << 	clusterVectorE.PseudoRapidity()<< " Rapidity="<<clusterVectorE.Rapidity() << " Phi=" <<clusterVectorE.Phi()<<" Px="<<clusterVectorE.Px()<<" Py="<<clusterVectorE.Py()<<" Pz="<<clusterVectorE.Pz()<<" E="<<clusterVectorE.E()<<" M="<<clusterVectorE.M()<<endl;

					//cout <<tCaloCluster_pt->at(i)<<" "<<tCaloCluster_eta->at(i)<<" "<<tCaloCluster_phi->at(i)<<" "<<tCaloCluster_m->at(i)<<" "<<tCaloCluster_e->at(i)<<endl;
					//clusterVectorM->SetPtEtaPhiM(tCaloCluster_pt->at(i),tCaloCluster_eta->at(i),tCaloCluster_phi->at(i),tCaloCluster_m->at(i));
					//cout <<"VectorM: "<< "PseudoRapidity=" << 	clusterVectorM->PseudoRapidity()<< " Rapidity="<<clusterVectorM->Rapidity() << " Phi=" <<clusterVectorM->Phi()<<" Px="<<clusterVectorM->Px()<<" Py="<<clusterVectorM->Py()<<" Pz="<<clusterVectorM->Pz()<<" E="<<clusterVectorM->E()<<" M="<<clusterVectorM->M()<<endl;

					//cout <<"DeltaR1="<< rndmCone1.DeltaR(clusterVectorE) <<endl;
					//cout <<"DeltaR1b="<< rndmCone1b.DeltaR(clusterVectorE) <<endl;
					if (TMath::Abs(rndmCone1.DeltaR(clusterVectorE))<RCone) {

					}
					//cout <<"DeltaR2="<< rndmCone2.DeltaR(clusterVectorE) <<endl;
					if (TMath::Abs(rndmCone2.DeltaR(clusterVectorE))<RCone) {

					}
					histoEtaPhi->Fill(tCaloCluster_phi->at(i),tCaloCluster_eta->at(i));
				}
				histoEtaPhi->Draw("col");
				TEllipse el1(phi1,eta1,RCone,RCone);
				el1.SetFillStyle(0);
				TEllipse el2(phi2,eta2,RCone,RCone);
				el2.SetFillStyle(0);
				el1.Draw();
				el2.Draw();

				c1->Print("EventViewerGood.png","png");
				counterGood++;
			}
			histoEnergyNVP->Fill(tNPV_n,pt1-pt2);
			return (pt1-pt2);
		} else {
			histoNPVMiss->Fill(tNPV_n);
			histoNearestConeMiss->Fill(smallestDistance);
			for (int i=0;i<tCaloCluster_pt->size();i++) {
				if (tCaloCluster_pt->at(i)<0) {
					continue;
				}
				histoEtaPhiMiss->Fill(tCaloCluster_phi->at(i),tCaloCluster_eta->at(i));
			}
			if (counterBad<1) {
				cout <<"rndmCone1: "<< "PseudoRapidity=" << 	rndmCone1.PseudoRapidity()<< " Rapidity="<<rndmCone1.Rapidity() << " Phi=" <<rndmCone1.Phi()<<" Px="<<rndmCone1.Px()<<" Py="<<rndmCone1.Py()<<" Pz="<<rndmCone1.Pz()<<" E="<<rndmCone1.E()<<" M="<<rndmCone1.M()<<endl;
				cout <<"rndmCone2: "<< "PseudoRapidity=" << 	rndmCone2.PseudoRapidity()<< " Rapidity="<<rndmCone2.Rapidity() << " Phi=" <<rndmCone2.Phi()<<" Px="<<rndmCone2.Px()<<" Py="<<rndmCone2.Py()<<" Pz="<<rndmCone2.Pz()<<" E="<<rndmCone2.E()<<" M="<<rndmCone2.M()<<endl;
				cout <<"VectorE: "<< "PseudoRapidity=" << 	clusterVectorE.PseudoRapidity()<< " Rapidity="<<clusterVectorE.Rapidity() << " Phi=" <<clusterVectorE.Phi()<<" Px="<<clusterVectorE.Px()<<" Py="<<clusterVectorE.Py()<<" Pz="<<clusterVectorE.Pz()<<" E="<<clusterVectorE.E()<<" M="<<clusterVectorE.M()<<endl;

				cout <<"DeltaR1="<< rndmCone1.DeltaR(clusterVectorE) <<endl;
				cout <<"DeltaR2="<< rndmCone2.DeltaR(clusterVectorE) <<endl;

				TCanvas *c1 = new TCanvas("c1");
				TH2F *histoEtaPhi = new TH2F("h","h",100,-3.2,3.2,100,-.8,.8);
				for (int i=0;i<tCaloCluster_pt->size();i++) {

					clusterVectorE.SetPtEtaPhiE(tCaloCluster_pt->at(i),tCaloCluster_eta->at(i),tCaloCluster_phi->at(i),tCaloCluster_e->at(i));
					//cout <<"VectorE: "<< "PseudoRapidity=" << 	clusterVectorE.PseudoRapidity()<< " Rapidity="<<clusterVectorE.Rapidity() << " Phi=" <<clusterVectorE.Phi()<<" Px="<<clusterVectorE.Px()<<" Py="<<clusterVectorE.Py()<<" Pz="<<clusterVectorE.Pz()<<" E="<<clusterVectorE.E()<<" M="<<clusterVectorE.M()<<endl;

					//cout <<tCaloCluster_pt->at(i)<<" "<<tCaloCluster_eta->at(i)<<" "<<tCaloCluster_phi->at(i)<<" "<<tCaloCluster_m->at(i)<<" "<<tCaloCluster_e->at(i)<<endl;
					//clusterVectorM->SetPtEtaPhiM(tCaloCluster_pt->at(i),tCaloCluster_eta->at(i),tCaloCluster_phi->at(i),tCaloCluster_m->at(i));
					//cout <<"VectorM: "<< "PseudoRapidity=" << 	clusterVectorM->PseudoRapidity()<< " Rapidity="<<clusterVectorM->Rapidity() << " Phi=" <<clusterVectorM->Phi()<<" Px="<<clusterVectorM->Px()<<" Py="<<clusterVectorM->Py()<<" Pz="<<clusterVectorM->Pz()<<" E="<<clusterVectorM->E()<<" M="<<clusterVectorM->M()<<endl;

					//cout <<"DeltaR1="<< rndmCone1.DeltaR(clusterVectorE) <<endl;
					//cout <<"DeltaR1b="<< rndmCone1b.DeltaR(clusterVectorE) <<endl;
					if (TMath::Abs(rndmCone1.DeltaR(clusterVectorE))<RCone) {

					}
					//cout <<"DeltaR2="<< rndmCone2.DeltaR(clusterVectorE) <<endl;
					if (TMath::Abs(rndmCone2.DeltaR(clusterVectorE))<RCone) {

					}

					histoEtaPhi->Fill(tCaloCluster_phi->at(i),tCaloCluster_eta->at(i));
				}
				histoEtaPhi->Draw("col");
				TEllipse el1(phi1,eta1,RCone,RCone);
				el1.SetFillStyle(0);
				TEllipse el2(phi2,eta2,RCone,RCone);
				el2.SetFillStyle(0);
				el1.Draw();
				el2.Draw();

				c1->Print("EventViewerNone.png","png");
				counterBad++;
			}
			histoEnergyNVP->Fill(tNPV_n,pt1-pt2);
			return (pt1-pt2);
		}
	} else {
		return 3000000.;
	}

	//return 3000000;
}
Double_t calculateDifference2(vector<float>* tCaloCluster_pt,vector<float>* tCaloCluster_e,vector<float>* tCaloCluster_m,vector<float>* tCaloCluster_phi,vector<float>* tCaloCluster_eta,Double_t lowerEta,Double_t upperEta,Double_t RCone,bool extraHist,TH1D* histoEta1,TH1D* histoEta2,TH1D* histoPhi1,TH1D* histoPhi2);
Double_t calculateDifference2(vector<float>* tCaloCluster_pt,vector<float>* tCaloCluster_e,vector<float>* tCaloCluster_m,vector<float>* tCaloCluster_phi,vector<float>* tCaloCluster_eta,Double_t lowerEta,Double_t upperEta,Double_t RCone,bool extraHist=false,TH1D* histoEta1=NULL,TH1D* histoEta2=NULL,TH1D* histoPhi1=NULL,TH1D* histoPhi2=NULL) {
	//histoFractionAtLeast1Pos,histoFractionAtLeast1PosFcnNumberPosClusters,histoFractionAtLeast1PosFcnNumberNegClusters,histoFractionAtLeast1PosFcnClusterPtInCone
	TRandom *r1=new TRandom1();
	r1->SetSeed(0);
	bool flagSomethingInCone1=false;
	bool flagSomethingInCone2=false;

	Double_t pt1=0;
	Double_t pt2=0;
	TLorentzVector clusterVectorE;
	TLorentzVector clusterVectorM;


	TLorentzVector rndmCone1;
	TLorentzVector rndmCone2;

	Double_t eta1;
	Double_t eta2;
	Double_t phi1;
	Double_t phi2;
	int maxTries=5;
	int numberOfTries=0;
	while ((flagSomethingInCone1==false||flagSomethingInCone2==false)&&numberOfTries<maxTries) {
		numberOfTries++;
		Double_t randomNumber=r1->Rndm();
		Double_t rangePhi=TMath::Pi()*2.;
		Double_t phi1=-TMath::Pi()+randomNumber*rangePhi;
		Double_t phi2;
		if (phi1>0) {
			phi2= phi1-TMath::Pi();
		} else {
			phi2=phi1+TMath::Pi();
		}

		//cout << "phi1="<< phi1 << " phi2="<<phi2<<endl;
		randomNumber=r1->Rndm();
		Double_t rangeEta=upperEta-lowerEta;
		Double_t eta1=lowerEta+randomNumber*rangeEta;
		randomNumber=r1->Rndm();
		Double_t eta2=lowerEta+randomNumber*rangeEta;

		//rndmCone1;
		//rndmCone2;

		rndmCone1.SetPtEtaPhiE(300,eta1,phi1,100);
		//rndmCone1b.SetPtEtaPhiE(4000.,eta1,phi1,3000.);
		rndmCone2.SetPtEtaPhiE(1000,eta2,phi2,3000);
		//cout <<"rndmCone1: "<< "PseudoRapidity=" << 	rndmCone1.PseudoRapidity()<< " Rapidity="<<rndmCone1.Rapidity() << " Phi=" <<rndmCone1.Phi()<<" Px="<<rndmCone1.Px()<<" Py="<<rndmCone1.Py()<<" Pz="<<rndmCone1.Pz()<<" E="<<rndmCone1.E()<<" M="<<rndmCone1.M()<<endl;
		//cout <<"rndmCone1b: "<< "PseudoRapidity=" << 	rndmCone1b.PseudoRapidity()<< " Rapidity="<<rndmCone1b.Rapidity() << " Phi=" <<rndmCone1b.Phi()<<" Px="<<rndmCone1b.Px()<<" Py="<<rndmCone1b.Py()<<" Pz="<<rndmCone1b.Pz()<<" E="<<rndmCone1b.E()<<" M="<<rndmCone1b.M()<<endl;
		//cout <<"rndmCone2: "<< "PseudoRapidity=" << 	rndmCone2.PseudoRapidity()<< " Rapidity="<<rndmCone2.Rapidity() << " Phi=" <<rndmCone2.Phi()<<" Px="<<rndmCone2.Px()<<" Py="<<rndmCone2.Py()<<" Pz="<<rndmCone2.Pz()<<" E="<<rndmCone2.E()<<" M="<<rndmCone2.M()<<endl;

		if (extraHist==true) {
			histoPhi1->Fill(phi1);
			histoPhi2->Fill(phi2);
			histoEta1->Fill(eta1);
			histoEta2->Fill(eta2);

		}

		//cout << "eta1="<< eta1 << " eta2="<<eta2<<endl;
		pt1=0;
		pt2=0;
		//TLorentzVector clusterVectorE;
		//TLorentzVector clusterVectorM;
		flagSomethingInCone1=false;
		flagSomethingInCone2=false;
		bool inBothCones=false;
		bool tempInCone1=false;
		bool tempInCone2=false;
		for (int i=0;i<tCaloCluster_pt->size();i++) {
			clusterVectorE.SetPtEtaPhiE(tCaloCluster_pt->at(i),tCaloCluster_eta->at(i),tCaloCluster_phi->at(i),tCaloCluster_e->at(i));
			//cout <<"VectorE: "<< "PseudoRapidity=" << 	clusterVectorE.PseudoRapidity()<< " Rapidity="<<clusterVectorE.Rapidity() << " Phi=" <<clusterVectorE.Phi()<<" Px="<<clusterVectorE.Px()<<" Py="<<clusterVectorE.Py()<<" Pz="<<clusterVectorE.Pz()<<" E="<<clusterVectorE.E()<<" M="<<clusterVectorE.M()<<endl;

			//cout <<tCaloCluster_pt->at(i)<<" "<<tCaloCluster_eta->at(i)<<" "<<tCaloCluster_phi->at(i)<<" "<<tCaloCluster_m->at(i)<<" "<<tCaloCluster_e->at(i)<<endl;
			//clusterVectorM->SetPtEtaPhiM(tCaloCluster_pt->at(i),tCaloCluster_eta->at(i),tCaloCluster_phi->at(i),tCaloCluster_m->at(i));
			//cout <<"VectorM: "<< "PseudoRapidity=" << 	clusterVectorM->PseudoRapidity()<< " Rapidity="<<clusterVectorM->Rapidity() << " Phi=" <<clusterVectorM->Phi()<<" Px="<<clusterVectorM->Px()<<" Py="<<clusterVectorM->Py()<<" Pz="<<clusterVectorM->Pz()<<" E="<<clusterVectorM->E()<<" M="<<clusterVectorM->M()<<endl;

			//cout <<"DeltaR1="<< rndmCone1.DeltaR(clusterVectorE) <<endl;
			//cout <<"DeltaR1b="<< rndmCone1b.DeltaR(clusterVectorE) <<endl;
			tempInCone1=false;
			tempInCone2=false;
			if (TMath::Abs(rndmCone1.DeltaR(clusterVectorE))<RCone) {
				pt1+=tCaloCluster_pt->at(i);
				flagSomethingInCone1=true;
				tempInCone1=true;

			}
			//cout <<"DeltaR2="<< rndmCone2.DeltaR(clusterVectorE) <<endl;
			if (TMath::Abs(rndmCone2.DeltaR(clusterVectorE))<RCone) {
				pt2+=tCaloCluster_pt->at(i);
				flagSomethingInCone2=true;
				tempInCone2=true;

			}
			if (tempInCone1==true&&tempInCone2==true) {
				cout <<"In both cones"<<endl;
			}
			/*if (tCaloCluster_pt->at(i)<0) {
				cout<<"                    HELLLO"<<endl;
			}*/
		}
	}
	if (numberOfTries<=maxTries&&flagSomethingInCone1==true&&flagSomethingInCone2==true) {
		/*if ((pt1-pt2)<.1&&(pt1-pt2)>-.1) {
			cout << "pt1="<< pt1<<" pt2="<<pt2<<endl;
		}*/
		return (pt1-pt2);

	} else {
		return 3000000.;
	}

	//return 3000000;
}
Double_t calculateDifference3(vector<float>* tCaloCluster_pt,vector<float>* tCaloCluster_e,vector<float>* tCaloCluster_m,vector<float>* tCaloCluster_phi,vector<float>* tCaloCluster_eta,Double_t lowerEta,Double_t upperEta,Double_t RCone,bool extraHist,TH1D* histoEta1,TH1D* histoEta2,TH1D* histoPhi1,TH1D* histoPhi2);
Double_t calculateDifference3(vector<float>* tCaloCluster_pt,vector<float>* tCaloCluster_e,vector<float>* tCaloCluster_m,vector<float>* tCaloCluster_phi,vector<float>* tCaloCluster_eta,Double_t lowerEta,Double_t upperEta,Double_t RCone,bool extraHist=false,TH1D* histoEta1=NULL,TH1D* histoEta2=NULL,TH1D* histoPhi1=NULL,TH1D* histoPhi2=NULL) {
	//histoFractionAtLeast1Pos,histoFractionAtLeast1PosFcnNumberPosClusters,histoFractionAtLeast1PosFcnNumberNegClusters,histoFractionAtLeast1PosFcnClusterPtInCone
	TRandom *r1=new TRandom1();
	r1->SetSeed(0);
	bool flagSomethingInCone1=false;
	bool flagSomethingInCone2=false;

	Double_t pt1=0;
	Double_t pt2=0;
	TLorentzVector clusterVectorE;
	TLorentzVector clusterVectorM;

	TLorentzVector ConeSum1;
	TLorentzVector ConeSum2;


	TLorentzVector rndmCone1;
	TLorentzVector rndmCone2;

	Double_t eta1;
	Double_t eta2;
	Double_t phi1;
	Double_t phi2;
	int maxTries=1;
	int numberOfTries=0;
	while ((flagSomethingInCone1==false||flagSomethingInCone2==false)&&numberOfTries<maxTries) {
		numberOfTries++;
		Double_t randomNumber=r1->Rndm();
		Double_t rangePhi=TMath::Pi()*2.;
		Double_t phi1=-TMath::Pi()+randomNumber*rangePhi;
		Double_t phi2;
		if (phi1>0) {
			phi2= phi1-TMath::Pi();
		} else {
			phi2=phi1+TMath::Pi();
		}

		//cout << "phi1="<< phi1 << " phi2="<<phi2<<endl;
		randomNumber=r1->Rndm();
		Double_t rangeEta=upperEta-lowerEta;
		Double_t eta1=lowerEta+randomNumber*rangeEta;
		randomNumber=r1->Rndm();
		Double_t eta2=lowerEta+randomNumber*rangeEta;

		//rndmCone1;
		//rndmCone2;

		rndmCone1.SetPtEtaPhiE(300,eta1,phi1,100);
		//rndmCone1b.SetPtEtaPhiE(4000.,eta1,phi1,3000.);
		rndmCone2.SetPtEtaPhiE(1000,eta2,phi2,3000);
		//cout <<"rndmCone1: "<< "PseudoRapidity=" << 	rndmCone1.PseudoRapidity()<< " Rapidity="<<rndmCone1.Rapidity() << " Phi=" <<rndmCone1.Phi()<<" Px="<<rndmCone1.Px()<<" Py="<<rndmCone1.Py()<<" Pz="<<rndmCone1.Pz()<<" E="<<rndmCone1.E()<<" M="<<rndmCone1.M()<<endl;
		//cout <<"rndmCone1b: "<< "PseudoRapidity=" << 	rndmCone1b.PseudoRapidity()<< " Rapidity="<<rndmCone1b.Rapidity() << " Phi=" <<rndmCone1b.Phi()<<" Px="<<rndmCone1b.Px()<<" Py="<<rndmCone1b.Py()<<" Pz="<<rndmCone1b.Pz()<<" E="<<rndmCone1b.E()<<" M="<<rndmCone1b.M()<<endl;
		//cout <<"rndmCone2: "<< "PseudoRapidity=" << 	rndmCone2.PseudoRapidity()<< " Rapidity="<<rndmCone2.Rapidity() << " Phi=" <<rndmCone2.Phi()<<" Px="<<rndmCone2.Px()<<" Py="<<rndmCone2.Py()<<" Pz="<<rndmCone2.Pz()<<" E="<<rndmCone2.E()<<" M="<<rndmCone2.M()<<endl;

		if (extraHist==true) {
			histoPhi1->Fill(phi1);
			histoPhi2->Fill(phi2);
			histoEta1->Fill(eta1);
			histoEta2->Fill(eta2);

		}

		//cout << "eta1="<< eta1 << " eta2="<<eta2<<endl;
		ConeSum1.SetXYZT(0,0,0,0);
		ConeSum2.SetXYZT(0,0,0,0);
		//pt1=0;
		//pt2=0;
		//TLorentzVector clusterVectorE;
		//TLorentzVector clusterVectorM;
		flagSomethingInCone1=false;
		flagSomethingInCone2=false;
		bool inBothCones=false;
		bool tempInCone1=false;
		bool tempInCone2=false;
		for (int i=0;i<tCaloCluster_pt->size();i++) {
			clusterVectorE.SetPtEtaPhiE(tCaloCluster_pt->at(i),tCaloCluster_eta->at(i),tCaloCluster_phi->at(i),tCaloCluster_e->at(i));
			//cout <<"VectorE: "<< "PseudoRapidity=" << 	clusterVectorE.PseudoRapidity()<< " Rapidity="<<clusterVectorE.Rapidity() << " Phi=" <<clusterVectorE.Phi()<<" Px="<<clusterVectorE.Px()<<" Py="<<clusterVectorE.Py()<<" Pz="<<clusterVectorE.Pz()<<" E="<<clusterVectorE.E()<<" M="<<clusterVectorE.M()<<endl;

			//cout <<tCaloCluster_pt->at(i)<<" "<<tCaloCluster_eta->at(i)<<" "<<tCaloCluster_phi->at(i)<<" "<<tCaloCluster_m->at(i)<<" "<<tCaloCluster_e->at(i)<<endl;
			//clusterVectorM->SetPtEtaPhiM(tCaloCluster_pt->at(i),tCaloCluster_eta->at(i),tCaloCluster_phi->at(i),tCaloCluster_m->at(i));
			//cout <<"VectorM: "<< "PseudoRapidity=" << 	clusterVectorM->PseudoRapidity()<< " Rapidity="<<clusterVectorM->Rapidity() << " Phi=" <<clusterVectorM->Phi()<<" Px="<<clusterVectorM->Px()<<" Py="<<clusterVectorM->Py()<<" Pz="<<clusterVectorM->Pz()<<" E="<<clusterVectorM->E()<<" M="<<clusterVectorM->M()<<endl;

			//cout <<"DeltaR1="<< rndmCone1.DeltaR(clusterVectorE) <<endl;
			//cout <<"DeltaR1b="<< rndmCone1b.DeltaR(clusterVectorE) <<endl;
			tempInCone1=false;
			tempInCone2=false;
			if (TMath::Abs(rndmCone1.DeltaR(clusterVectorE))<RCone) {
				ConeSum1=ConeSum1+clusterVectorE;
				flagSomethingInCone1=true;
				tempInCone1=true;

			}
			//cout <<"DeltaR2="<< rndmCone2.DeltaR(clusterVectorE) <<endl;
			if (TMath::Abs(rndmCone2.DeltaR(clusterVectorE))<RCone) {
				ConeSum2=ConeSum2+clusterVectorE;
				flagSomethingInCone2=true;
				tempInCone2=true;

			}
			if (tempInCone1==true&&tempInCone2==true) {
				cout <<"In both cones"<<endl;
			}
			/*if (tCaloCluster_pt->at(i)<0) {
				cout<<"                    HELLLO"<<endl;
			}*/
		}
	}
	if (numberOfTries<=maxTries&&flagSomethingInCone1==true&&flagSomethingInCone2==true) {
		/*if ((pt1-pt2)<.1&&(pt1-pt2)>-.1) {
			cout << "pt1="<< pt1<<" pt2="<<pt2<<endl;
		}*/
		return (ConeSum1.Pt()-ConeSum2.Pt());

	} else {
		return 3000000.;
	}

	//return 3000000;
}
void calculateDifference3SymConeExample(vector<float>* tCaloCluster_pt,vector<float>* tCaloCluster_e,vector<float>* tCaloCluster_m,vector<float>* tCaloCluster_phi,vector<float>* tCaloCluster_eta,Double_t lowerEta,Double_t upperEta,Double_t RCone,int name,bool extraHist,TH1D* histoEta1,TH1D* histoEta2,TH1D* histoPhi1,TH1D* histoPhi2);
void calculateDifference3SymConeExample(vector<float>* tCaloCluster_pt,vector<float>* tCaloCluster_e,vector<float>* tCaloCluster_m,vector<float>* tCaloCluster_phi,vector<float>* tCaloCluster_eta,Double_t lowerEta,Double_t upperEta,Double_t RCone,int name,bool extraHist=false,TH1D* histoEta1=NULL,TH1D* histoEta2=NULL,TH1D* histoPhi1=NULL,TH1D* histoPhi2=NULL) {
	//histoFractionAtLeast1Pos,histoFractionAtLeast1PosFcnNumberPosClusters,histoFractionAtLeast1PosFcnNumberNegClusters,histoFractionAtLeast1PosFcnClusterPtInCone
	TRandom *r1=new TRandom1();
	r1->SetSeed(0);
	bool flagSomethingInCone1=false;
	bool flagSomethingInCone2=false;

	Double_t pt1=0;
	Double_t pt2=0;




	Double_t randomNumber=r1->Rndm();
	Double_t rangePhi=TMath::Pi()*2.;
	Double_t phi1=-TMath::Pi()+randomNumber*rangePhi;
	Double_t phi2;
	if (phi1>0) {
		phi2= phi1-TMath::Pi();
	} else {
		phi2=phi1+TMath::Pi();
	}

	//cout << "phi1="<< phi1 << " phi2="<<phi2<<endl;
	randomNumber=r1->Rndm();
	Double_t eta1;
	Double_t eta2;
	Double_t rangeEta=(upperEta-lowerEta)*2;
	if (randomNumber<.5) {
		eta1=-upperEta+randomNumber*rangeEta;
	} else {
		eta1=lowerEta+(randomNumber-.5)*rangeEta;
	}

	randomNumber=r1->Rndm();
	if (randomNumber<.5) {
		eta2=-upperEta+randomNumber*rangeEta;
	} else {
		eta2=lowerEta+(randomNumber-.5)*rangeEta;
	}





	TCanvas *c1 = new TCanvas("c1");
	TH2F *histoEtaPhi = new TH2F("h","h",640,-3.2,3.2,920,-4.6,4.6);
	for (int i=0;i<tCaloCluster_pt->size();i++) {
		histoEtaPhi->Fill(tCaloCluster_phi->at(i),tCaloCluster_eta->at(i));
	}
	histoEtaPhi->Draw("col");
	TEllipse el1(phi1,eta1,RCone,RCone);
	el1.SetFillStyle(0);
	TEllipse el2(phi2,eta2,RCone,RCone);
	el2.SetFillStyle(0);
	el1.Draw();
	el2.Draw();

	TLine *line1=new TLine(histoEtaPhi->GetXaxis()->GetXmin(),lowerEta,histoEtaPhi->GetXaxis()->GetXmax(),lowerEta);
	line1->SetLineColor(kBlue);
	line1->Draw();
	TLine *line2=new TLine(histoEtaPhi->GetXaxis()->GetXmin(),upperEta,histoEtaPhi->GetXaxis()->GetXmax(),upperEta);
	line2->SetLineColor(kBlue);
	line2->Draw();
	TLine *line3=new TLine(histoEtaPhi->GetXaxis()->GetXmin(),-lowerEta,histoEtaPhi->GetXaxis()->GetXmax(),-lowerEta);
	line3->SetLineColor(kBlue);
	line3->Draw();
	TLine *line4=new TLine(histoEtaPhi->GetXaxis()->GetXmin(),-upperEta,histoEtaPhi->GetXaxis()->GetXmax(),-upperEta);
	line4->SetLineColor(kBlue);
	line4->Draw();
	c1->Print(("EventViewer"+std::to_string(name)+".png").c_str(),"png");
}
Double_t calculateDifference3Sym(vector<float>* tCaloCluster_pt,vector<float>* tCaloCluster_e,vector<float>* tCaloCluster_m,vector<float>* tCaloCluster_phi,vector<float>* tCaloCluster_eta,Double_t lowerEta,Double_t upperEta,Double_t RCone,int maxTries,bool extraHist,TH1D* histoEta1,TH1D* histoEta2,TH1D* histoPhi1,TH1D* histoPhi2);
Double_t calculateDifference3Sym(vector<float>* tCaloCluster_pt,vector<float>* tCaloCluster_e,vector<float>* tCaloCluster_m,vector<float>* tCaloCluster_phi,vector<float>* tCaloCluster_eta,Double_t lowerEta,Double_t upperEta,Double_t RCone,int maxTries=1,bool extraHist=false,TH1D* histoEta1=NULL,TH1D* histoEta2=NULL,TH1D* histoPhi1=NULL,TH1D* histoPhi2=NULL) {
	//histoFractionAtLeast1Pos,histoFractionAtLeast1PosFcnNumberPosClusters,histoFractionAtLeast1PosFcnNumberNegClusters,histoFractionAtLeast1PosFcnClusterPtInCone
	TRandom *r1=new TRandom1();
	r1->SetSeed(0);
	bool flagSomethingInCone1=false;
	bool flagSomethingInCone2=false;

	Double_t pt1=0;
	Double_t pt2=0;
	TLorentzVector clusterVectorE;
	TLorentzVector clusterVectorM;

	TLorentzVector ConeSum1;
	TLorentzVector ConeSum2;


	TLorentzVector rndmCone1;
	TLorentzVector rndmCone2;

	Double_t eta1;
	Double_t eta2;
	Double_t phi1;
	Double_t phi2;

	int numberOfTries=0;
	while ((flagSomethingInCone1==false||flagSomethingInCone2==false)&&numberOfTries<maxTries) {
		numberOfTries++;
		Double_t randomNumber=r1->Rndm();
		Double_t rangePhi=TMath::Pi()*2.;
		Double_t phi1=-TMath::Pi()+randomNumber*rangePhi;
		Double_t phi2;
		if (phi1>0) {
			phi2= phi1-TMath::Pi();
		} else {
			phi2=phi1+TMath::Pi();
		}

		//cout << "phi1="<< phi1 << " phi2="<<phi2<<endl;
		randomNumber=r1->Rndm();
		Double_t eta1;
		Double_t eta2;
		Double_t rangeEta=(upperEta-lowerEta)*2;
		if (randomNumber<.5) {
			eta1=-upperEta+randomNumber*rangeEta;
		} else {
			eta1=lowerEta+(randomNumber-.5)*rangeEta;
		}

		randomNumber=r1->Rndm();
		if (randomNumber<.5) {
			eta2=-upperEta+randomNumber*rangeEta;
		} else {
			eta2=lowerEta+(randomNumber-.5)*rangeEta;
		}

		//rndmCone1;
		//rndmCone2;

		rndmCone1.SetPtEtaPhiE(300,eta1,phi1,100);
		//rndmCone1b.SetPtEtaPhiE(4000.,eta1,phi1,3000.);
		rndmCone2.SetPtEtaPhiE(1000,eta2,phi2,3000);
		//cout <<"rndmCone1: "<< "PseudoRapidity=" << 	rndmCone1.PseudoRapidity()<< " Rapidity="<<rndmCone1.Rapidity() << " Phi=" <<rndmCone1.Phi()<<" Px="<<rndmCone1.Px()<<" Py="<<rndmCone1.Py()<<" Pz="<<rndmCone1.Pz()<<" E="<<rndmCone1.E()<<" M="<<rndmCone1.M()<<endl;
		//cout <<"rndmCone1b: "<< "PseudoRapidity=" << 	rndmCone1b.PseudoRapidity()<< " Rapidity="<<rndmCone1b.Rapidity() << " Phi=" <<rndmCone1b.Phi()<<" Px="<<rndmCone1b.Px()<<" Py="<<rndmCone1b.Py()<<" Pz="<<rndmCone1b.Pz()<<" E="<<rndmCone1b.E()<<" M="<<rndmCone1b.M()<<endl;
		//cout <<"rndmCone2: "<< "PseudoRapidity=" << 	rndmCone2.PseudoRapidity()<< " Rapidity="<<rndmCone2.Rapidity() << " Phi=" <<rndmCone2.Phi()<<" Px="<<rndmCone2.Px()<<" Py="<<rndmCone2.Py()<<" Pz="<<rndmCone2.Pz()<<" E="<<rndmCone2.E()<<" M="<<rndmCone2.M()<<endl;

		if (extraHist==true) {
			histoPhi1->Fill(phi1);
			histoPhi2->Fill(phi2);
			histoEta1->Fill(eta1);
			histoEta2->Fill(eta2);

		}

		//cout << "eta1="<< eta1 << " eta2="<<eta2<<endl;
		ConeSum1.SetXYZT(0,0,0,0);
		ConeSum2.SetXYZT(0,0,0,0);
		//pt1=0;
		//pt2=0;
		//TLorentzVector clusterVectorE;
		//TLorentzVector clusterVectorM;
		flagSomethingInCone1=false;
		flagSomethingInCone2=false;
		bool inBothCones=false;
		bool tempInCone1=false;
		bool tempInCone2=false;
		for (int i=0;i<tCaloCluster_pt->size();i++) {
			clusterVectorE.SetPtEtaPhiE(tCaloCluster_pt->at(i),tCaloCluster_eta->at(i),tCaloCluster_phi->at(i),tCaloCluster_e->at(i));
			//cout <<"VectorE: "<< "PseudoRapidity=" << 	clusterVectorE.PseudoRapidity()<< " Rapidity="<<clusterVectorE.Rapidity() << " Phi=" <<clusterVectorE.Phi()<<" Px="<<clusterVectorE.Px()<<" Py="<<clusterVectorE.Py()<<" Pz="<<clusterVectorE.Pz()<<" E="<<clusterVectorE.E()<<" M="<<clusterVectorE.M()<<endl;

			//cout <<tCaloCluster_pt->at(i)<<" "<<tCaloCluster_eta->at(i)<<" "<<tCaloCluster_phi->at(i)<<" "<<tCaloCluster_m->at(i)<<" "<<tCaloCluster_e->at(i)<<endl;
			//clusterVectorM->SetPtEtaPhiM(tCaloCluster_pt->at(i),tCaloCluster_eta->at(i),tCaloCluster_phi->at(i),tCaloCluster_m->at(i));
			//cout <<"VectorM: "<< "PseudoRapidity=" << 	clusterVectorM->PseudoRapidity()<< " Rapidity="<<clusterVectorM->Rapidity() << " Phi=" <<clusterVectorM->Phi()<<" Px="<<clusterVectorM->Px()<<" Py="<<clusterVectorM->Py()<<" Pz="<<clusterVectorM->Pz()<<" E="<<clusterVectorM->E()<<" M="<<clusterVectorM->M()<<endl;

			//cout <<"DeltaR1="<< rndmCone1.DeltaR(clusterVectorE) <<endl;
			//cout <<"DeltaR1b="<< rndmCone1b.DeltaR(clusterVectorE) <<endl;
			tempInCone1=false;
			tempInCone2=false;
			if (TMath::Abs(rndmCone1.DeltaR(clusterVectorE))<RCone) {
				ConeSum1=ConeSum1+clusterVectorE;
				flagSomethingInCone1=true;
				tempInCone1=true;

			}
			//cout <<"DeltaR2="<< rndmCone2.DeltaR(clusterVectorE) <<endl;
			if (TMath::Abs(rndmCone2.DeltaR(clusterVectorE))<RCone) {
				ConeSum2=ConeSum2+clusterVectorE;
				flagSomethingInCone2=true;
				tempInCone2=true;

			}
			if (tempInCone1==true&&tempInCone2==true) {
				cout <<"In both cones"<<endl;
			}
			/*if (tCaloCluster_pt->at(i)<0) {
				cout<<"                    HELLLO"<<endl;
			}*/
		}
	}
	if (numberOfTries<=maxTries&&flagSomethingInCone1==true&&flagSomethingInCone2==true) {
		/*if ((pt1-pt2)<.1&&(pt1-pt2)>-.1) {
			cout << "pt1="<< pt1<<" pt2="<<pt2<<endl;
		}*/
		return (ConeSum1.Pt()-ConeSum2.Pt());

	} else {
		return 3000000.;
	}

	//return 3000000;
}
Double_t getWidth(TH1D* histo) {
	int mean_bin = histo->FindBin(histo->GetMean());
	cout<< "mean_bin="<<mean_bin<<" mean="<<histo->GetMean()<<endl;

    int binup = mean_bin;
    int bindown = mean_bin-1;
    Double_t total = histo->Integral();
    Double_t runningup = 0;
    Double_t runningdown = 0;
    if (total<50) {
		return -10.;
	}
    while (runningup < 0.6827/2.0*total) {
        runningup += histo->GetBinContent(binup);
        binup += 1;
	}
    while (runningdown < 0.6827/2.0*total) {
        runningdown += histo->GetBinContent(bindown);
        bindown -= 1;
	}

    Double_t width = (histo->GetBinCenter(binup)-histo->GetBinCenter(bindown));
    return width/1000.;
}
void saveSingleHisto(TH1* histo,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale=false,bool rebin=false);
void saveSingleHisto(TH1* histo,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale,bool rebin) {
	TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);

	if (logScale) {
		//gPad->SetLogy(1);
		//c1->SetLogy(1);
		c1->SetLogy();
	}
	TLatex latex;
	latex.SetNDC();
	latex.SetTextColor(1);
	latex.SetTextFont(42);
	latex.SetTextSize(.04);

	//Double_t scale=-1.;
	//cout<<"after going through stack"<<endl;
	/*if (unitArea) {
		stack->Scale(1/(stack->Integral()));
		dataHisto->Scale(1/(dataHisto->Integral()));
		stuffToWrite.push_back("scaled to unit area");
	}*/
	if (normalizedArea) {
		/*cout<<stack->GetNbinsX()<<" " <<dataHisto->GetNbinsX()<<endl;
		cout<<stack->Integral()<< " " << dataHisto->Integral()<<endl;
		stack->Scale(dataHisto->Integral()/(stack->Integral()));
		cout<<stack->Integral()<<endl;
		//dataHisto->Scale(1/(dataHisto->Integral()));*/

		cout<<histo->GetNbinsX()<<endl;
		cout<<histo->Integral(1,histo->GetNbinsX())<<endl;
		histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
		cout<<histo->Integral(1,histo->GetNbinsX())<<endl;
		//dataHisto->Scale(1/(dataHisto->Integral()));
		//stuffToWrite.push_back("Period A4");
	}
	if (rebin) {
		histo->Rebin();
	}

	histo->Draw();
	//dataHisto->GetYaxis()->SetRange(100000,9e6);
	//dataHisto->SetAxisRange(yAxisStart,yAxisEnd,"Y");
	//dataHisto->GetYaxis()->SetRangeUser(0,10e9);
	if (logScale) {
		histo->SetMinimum(.2);
		histo->SetMaximum(histo->GetMaximum()*1000);
	} else {
		/*dataHisto->SetMinimum(1);
		if (rebin) {
			dataHisto->SetMaximum(dataHisto->GetMaximum()*1.9);
		} else {
			dataHisto->SetMaximum(dataHisto->GetMaximum()*1.6);
		}*/
	}
	histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
	histo->GetXaxis()->SetLabelSize(0.04);
	histo->GetXaxis()->SetNdivisions(505);
	histo->GetXaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
	histo->GetYaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitleOffset(1.75);
	//dataHisto->GetYaxis()->SetLabelSize(.04);
	histo->GetYaxis()->SetLabelSize(.04);

	//int colourAdd=4;
	//int fillStyle=3005;
    //stack->SetLineColor(colourAdd);
   // stack->SetFillColor(colourAdd);
    //stack->SetFillStyle(fillStyle);
	//stack->SetLineColor(1);
	//stack->SetFillStyle(0);
	//stack->Draw("same HIST e");

	//stack->GetXaxis()->SetTitle(xaxisTitle.c_str());
	//stack->GetXaxis()->SetLabelSize(.04);
	//stack->GetYaxis()->SetTitle(yaxisTitle.c_str());
	//stack->GetYaxis()->SetLabelSize(.04);
	//gStyle->SetErrorX(0);
	//dataHisto->Draw("same ERR");
	//cout<<"before draw"<<endl;
	//legend->SetX1(.0);
	/*TLegend *legend2;
	//legend2 = new TLegend(.18,.82,1.,.92);
	legend2 = new TLegend(.70,.75,1.,.92);
	legend2->SetFillColor(0);
	legend2->SetFillStyle(0);//make it transparent
	legend2->SetBorderSize(0);
	legend2->SetTextSize(0.08);
	//legend->SetTextAlign(13);
	legend2->SetMargin(.15);
	legend2->AddEntry(dataHisto,"data","p");
	legend2->AddEntry(stack,"Pythia8","f");
	legend2->Draw();*/

	//ATLASLabel(0.48,0.88,"Internal");
	//ATLASLabel3(xStart,yStart,"Internal",1.05,.08);
	ATLASLabel3(xStart,yStart,"Internal",.85,.04);
	Double_t x=xStart;Double_t y=yStart-.045;
	for (int j=0;j<stuffToWrite.size();j++) {
		latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
		y-=.045;
	}
	/*pad2->cd();
	TH1F *h1_ratio = (TH1F*)dataHisto->Clone();
	h1_ratio->Add(h1_ratio,stack,1,-1);
	h1_ratio->Divide(stack);
	//h1_ratio->GetYaxis()->SetRangeUser(0, 2.);
	h1_ratio->GetYaxis()->SetRangeUser(-.15, .15);
	h1_ratio->GetYaxis()->SetTitle("#splitline{Relative}{Difference}");
	h1_ratio->SetMaximum(.19);
	h1_ratio->SetMinimum(-.19);
	h1_ratio->GetYaxis()->SetTitleSize(.12);
	h1_ratio->GetYaxis()->SetTitleOffset(.70);
	h1_ratio->GetYaxis()->SetLabelSize(.11);
	h1_ratio->GetXaxis()->SetTitle(xaxisTitle.c_str());
	h1_ratio->GetXaxis()->SetLabelSize(.11);
	h1_ratio->GetXaxis()->SetTitleOffset(1.1);*/
	/*h1_ratio->GetYaxis()->SetTitleSize(.08);
	h1_ratio->GetYaxis()->SetTitleOffset(.8);
	h1_ratio->GetYaxis()->SetLabelSize(.08);
	h1_ratio->GetXaxis()->SetTitle(xaxisTitle.c_str());
	h1_ratio->GetXaxis()->SetLabelSize(.08);
	h1_ratio->GetXaxis()->SetTitleOffset(.8);*/
	//h1_ratio->GetYaxis()->SetNdivisions(10);
	//h1_ratio->GetYaxis()->SetTitleOffset(1.85);
	//h1_ratio->GetYaxis()->SetTitleFont(43);
	//h1_ratio->GetYaxis()->SetLabelSize(0);
	//h1_ratio->GetYaxis()->SetTitleSize(28);
	//h1_ratio->GetXaxis()->SetLabelFont(43);
	//h1_ratio->GetXaxis()->SetTitleFont(43);
	//h1_ratio->GetXaxis()->SetLabelSize(28);
	//h1_ratio->GetXaxis()->SetTitleSize(.12);
	//h1_ratio->GetXaxis()->SetRangeUser(xmin, xmax);
	//TGaxis *ratioaxis = new TGaxis(xmin, 0.5, xmin, 1.5, 0.5, 1.5, 503, "");
	//ratioaxis->SetLabelFont(43);
	//ratioaxis->SetLabelSize(28);

	//h1_ratio->Draw("ERR");
	//TLine *line=new TLine(h1_ratio->GetXaxis()->GetXmin(),0,h1_ratio->GetXaxis()->GetXmax(),0);
	//line->SetLineStyle(7);
	//line->Draw();
	//ratioaxis->Draw();
	//cout<<"before save"<<endl;
	std::string temp(histo->GetName());
	if (rebin) {
		temp=temp+"rebin";
	}
	c1->Print((outPutDir+temp+".png").c_str(),"png");
	c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");

	//cout<<gPad->GetWh()<<endl;
	//cout<<gPad->GetWw()<<endl;
	//h->Draw();
}
void makePlotsRanCone4(const std::string& runNumberString,const std::string& outputFilename) {
	//TEnv *config = new TEnv("/share/home/khildebr/qualTask/run/makePlotsRanCone4.config");
	TEnv *config = new TEnv("makePlotsRanCone4.config");
	if ( !config ) {
		printf("configFile=makePlotsRanCone4.config doesn't appear to exists\n");
	}

	bool isMC = config->GetValue("isMC",false);
	bool muScale = config->GetValue("muScale",false);
	bool newNames = config->GetValue("newNames",true);
	bool doConesExample = config->GetValue("doConesExample",false);
	//bool useAddedFile = config->GetValue("useAddedFile",true);
	bool doSecondary = config->GetValue("doSecondary",false);
	int maxNPV = config->GetValue("maxNPV",30);
	int maxMu = config->GetValue("maxMu",35);
	int minNPV = config->GetValue("minNPV",0);
	int minMu = config->GetValue("minMu",0);
	int nBinsForA = config->GetValue("nBinsForA",1000);
	int range = config->GetValue("range",20000);
	cout<<"maxNPV= "<<maxNPV<<endl;
	cout<<"minNPV= "<<minNPV<<endl;
	cout<<"maxMu= "<<maxMu<<endl;
	cout<<"minMu= "<<minMu<<endl;
	cout<<"range= "<<range<<endl;
	cout<<"isMC= "<<isMC<<endl;
	//int nEtaSteps = config->GetValue("nEtaSteps",1);
	//int nEtaStepsSecondary = config->GetValue("nEtaStepsSecondary",1);
	std::string etaBlocksString= config->GetValue("etaBlocks","0,.8");
	std::string etaBlocksSecondaryString= config->GetValue("etaBlocksSecondary","0,2.1");

	std::string temp;

	int nEtaSteps=-1;
	printf("etaBlocksString=%s\n",etaBlocksString.c_str());
	stringstream ss(etaBlocksString);
	vector<double> etaBlocks;
	while(getline(ss,temp,',')) {
		etaBlocks.push_back(std::stod(temp));
		nEtaSteps++;
	}
	for (int i=0;i<etaBlocks.size();i++) {
		printf("etaBlocks[%i]=%f\n",i,etaBlocks[i]);
	}
	int nEtaStepsSecondary=-1;
	printf("etaBlocksSecondaryString=%s\n",etaBlocksSecondaryString.c_str());
	stringstream ss2(etaBlocksSecondaryString);
	vector<double> etaBlocksSecondary;
	while(getline(ss2,temp,',')) {
		etaBlocksSecondary.push_back(std::stod(temp));
		nEtaStepsSecondary++;
	}
	for (int i=0;i<etaBlocksSecondary.size();i++) {
		printf("etaBlocksSecondary[%i]=%f\n",i,etaBlocksSecondary[i]);
	}
	SetAtlasStyle();
	gStyle->SetHistLineWidth(1.0);
	gStyle->SetMarkerSize(1.0);
	//Style->SetMarkerStyle(20);
	//gStyle->SetMarkerColor(1);
  	gStyle->SetPalette(1);

	TFile *inFile=new TFile((runNumberString).c_str());
	TTree *inTree=(TTree*)inFile->Get("CaloCalTopoClusters");
	/*
	TChain *chain=new TChain("CaloCalTopoClusters");
	chain->Add((runNumberString+"*.root").c_str());
	*/

	vector<float>* tCaloCluster_EM_rapidity=new vector<float>();
	vector<float>* tCaloCluster_EM_pt=new vector<float>();
	vector<float>* tCaloCluster_EM_e=new vector<float>();
	vector<float>* tCaloCluster_EM_et=new vector<float>();
	vector<float>* tCaloCluster_EM_phi=new vector<float>();
	vector<float>* tCaloCluster_EM_eta=new vector<float>();
	vector<float>* tCaloCluster_EM_m=new vector<float>();

	vector<float>* tCaloCluster_LC_rapidity=new vector<float>();
	vector<float>* tCaloCluster_LC_pt=new vector<float>();
	vector<float>* tCaloCluster_LC_e=new vector<float>();
	vector<float>* tCaloCluster_LC_et=new vector<float>();
	vector<float>* tCaloCluster_LC_phi=new vector<float>();
	vector<float>* tCaloCluster_LC_eta=new vector<float>();
	vector<float>* tCaloCluster_LC_m=new vector<float>();

	int tCaloCluster_n;
	int tNPV_n;
	float taverageMu;
	float tcorrectedAverageMu;
	float tEventWeight;
	float tPileupWeight;


	if (newNames==true) {
		inTree->SetBranchAddress("CaloCalTopoClusters_EM_pt",&tCaloCluster_EM_pt);
		inTree->SetBranchAddress("CaloCalTopoClusters_EM_rapidity",&tCaloCluster_EM_rapidity);
		inTree->SetBranchAddress("CaloCalTopoClusters_EM_e",&tCaloCluster_EM_e);
		inTree->SetBranchAddress("CaloCalTopoClusters_EM_et",&tCaloCluster_EM_et);
		inTree->SetBranchAddress("CaloCalTopoClusters_EM_phi",&tCaloCluster_EM_phi);
		inTree->SetBranchAddress("CaloCalTopoClusters_EM_eta",&tCaloCluster_EM_eta);
		inTree->SetBranchAddress("CaloCalTopoClusters_EM_m",&tCaloCluster_EM_m);

		inTree->SetBranchAddress("CaloCalTopoClusters_LC_pt",&tCaloCluster_LC_pt);
		inTree->SetBranchAddress("CaloCalTopoClusters_LC_rapidity",&tCaloCluster_LC_rapidity);
		inTree->SetBranchAddress("CaloCalTopoClusters_LC_e",&tCaloCluster_LC_e);
		inTree->SetBranchAddress("CaloCalTopoClusters_LC_et",&tCaloCluster_LC_et);
		inTree->SetBranchAddress("CaloCalTopoClusters_LC_phi",&tCaloCluster_LC_phi);
		inTree->SetBranchAddress("CaloCalTopoClusters_LC_eta",&tCaloCluster_LC_eta);
		inTree->SetBranchAddress("CaloCalTopoClusters_LC_m",&tCaloCluster_LC_m);

		inTree->SetBranchAddress("CaloCalTopoClusters_n",&tCaloCluster_n);

		inTree->SetBranchAddress("PrimaryVertices_n",&tNPV_n);
	} else {
		inTree->SetBranchAddress("CaloCluster_EM_pt",&tCaloCluster_EM_pt);
		inTree->SetBranchAddress("CaloCluster_EM_rapidity",&tCaloCluster_EM_rapidity);
		inTree->SetBranchAddress("CaloCluster_EM_e",&tCaloCluster_EM_e);
		inTree->SetBranchAddress("CaloCluster_EM_et",&tCaloCluster_EM_et);
		inTree->SetBranchAddress("CaloCluster_EM_phi",&tCaloCluster_EM_phi);
		inTree->SetBranchAddress("CaloCluster_EM_eta",&tCaloCluster_EM_eta);
		inTree->SetBranchAddress("CaloCluster_EM_m",&tCaloCluster_EM_m);

		inTree->SetBranchAddress("CaloCluster_LC_pt",&tCaloCluster_LC_pt);
		inTree->SetBranchAddress("CaloCluster_LC_rapidity",&tCaloCluster_LC_rapidity);
		inTree->SetBranchAddress("CaloCluster_LC_e",&tCaloCluster_LC_e);
		inTree->SetBranchAddress("CaloCluster_LC_et",&tCaloCluster_LC_et);
		inTree->SetBranchAddress("CaloCluster_LC_phi",&tCaloCluster_LC_phi);
		inTree->SetBranchAddress("CaloCluster_LC_eta",&tCaloCluster_LC_eta);
		inTree->SetBranchAddress("CaloCluster_LC_m",&tCaloCluster_LC_m);

		inTree->SetBranchAddress("CaloCluster_n",&tCaloCluster_n);

		inTree->SetBranchAddress("Primary_Vertices_n",&tNPV_n);

		//inTree->SetBranchAddress("averageMu",&taverageMu);

	}
	inTree->SetBranchAddress("MCEventWeight",&tEventWeight);
	inTree->SetBranchAddress("PileupWeight",&tPileupWeight);
	inTree->SetBranchAddress("averageMu",&taverageMu);
	inTree->SetBranchAddress("correctedAverageMu",&tcorrectedAverageMu);

	int nEntries = inTree->GetEntries(); // Get the number of entries in this tree
	//cout<<"before entering for loop"<<endl;
	cout<<"nEntries="<<nEntries<<endl;
	cout<<"runNumberInfo="<<runNumberString<<endl;
	//Double_t weightHist;
	//Double_t weighthistWOPU;


//	int maxNPV=35;
//	int minNPV=0;

//	int nEtaSteps=7;
//	Double_t etaBlocks[]={0,.8,1.2,2.1,2.8,3.2,3.6,4.5};

//	bool doSecondary=true;
	//int nEtaStepsSecondary=7;
	//Double_t etaBlocksSecondary[]={2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8};
//	int nEtaStepsSecondary=1;
//	Double_t etaBlocksSecondary[]={0,2.1};
//	int nBinsForA=1000;
//	int range=20000;
//	int maxMu=35;
//	int minMu=0;
	TH3D *R4ConeEM_etaSlicesPos[nEtaSteps];

	TH3D *R4ConeLC_etaSlicesPos[nEtaSteps];

	TH3D *R4ConeEM_etaSlicesSecondary[nEtaStepsSecondary];

	TH3D *R4ConeLC_etaSlicesSecondary[nEtaStepsSecondary];
	//TH1D *Eta1Test=new TH1D("eta1","eta1",200,-1.3,1.3);
	//TH1D *Eta2Test=new TH1D("eta2","eta2",200,-1.3,1.3);
	//TH1D *Phi1Test=new TH1D("phi1","phi1",200,-3.2,3.2);
	//TH1D *Phi2Test=new TH1D("phi2","phi2",200,-3.2,3.2);
	TH1D *averageMuHisto=new TH1D("averageMuHisto","averageMuHisto",maxMu-minMu,minMu,maxMu);
	TH1D *correctedAverageMuHisto=new TH1D("correctedAverageMuHisto","correctedAverageMuHisto",maxMu-minMu,minMu,maxMu);
	TH1D *NPVHisto=new TH1D("NPVHisto","NPVHisto",maxNPV-minNPV,minNPV,maxNPV);
	TH1D *emptyConesSkippedHistoEM[nEtaSteps];
	TH1D *emptyConesSkippedHistoLC[nEtaSteps];
	for (int x=0;x<nEtaSteps;x++) {
		R4ConeEM_etaSlicesPos[x]=new TH3D(("R4ConeEM_etaSlicesPos"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),"R=.4 EM Scale",nBinsForA,-range,range,maxNPV-minNPV,minNPV,maxNPV,maxMu-minMu,minMu,maxMu);

		R4ConeLC_etaSlicesPos[x]=new TH3D(("R4ConeLC_etaSlicesPos"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),"R=.4 LC Scale",nBinsForA,-range,range,maxNPV-minNPV,minNPV,maxNPV,maxMu-minMu,minMu,maxMu);

		emptyConesSkippedHistoEM[x]=new TH1D(("emptyConesSkippedHistoEM"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),"emptyConesSkippedHistoEM",2,0,2);
		emptyConesSkippedHistoLC[x]=new TH1D(("emptyConesSkippedHistoLC"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),"emptyConesSkippedHistoLC",2,0,2);

	}
	if (doSecondary==true) {
		for (int x=0;x<nEtaStepsSecondary;x++) {
			R4ConeEM_etaSlicesSecondary[x]=new TH3D(("R4ConeEM_etaSlicesSecondary"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),"R=.4 EM Scale",nBinsForA,-range,range,maxNPV-minNPV,minNPV,maxNPV,maxMu-minMu,minMu,maxMu);

			R4ConeLC_etaSlicesSecondary[x]=new TH3D(("R4ConeLC_etaSlicesSecondary"+std::to_string(x)+"_"+std::to_string(x+1)).c_str(),"R=.4 LC Scale",nBinsForA,-range,range,maxNPV-minNPV,minNPV,maxNPV,maxMu-minMu,minMu,maxMu);

		}
	}
	Double_t tempDifference=0;
	Double_t weight=1.;
	//Double_t etaStepSize=.3;
	if (doConesExample==true) {
		for (int iEnt = 0; iEnt < 10; iEnt++) {
			inTree->GetEntry(iEnt);
			calculateDifference3SymConeExample(tCaloCluster_EM_pt,tCaloCluster_EM_e,tCaloCluster_EM_m,tCaloCluster_EM_phi,tCaloCluster_EM_eta,etaBlocks[0],etaBlocks[1],.4,iEnt);
			calculateDifference3SymConeExample(tCaloCluster_EM_pt,tCaloCluster_EM_e,tCaloCluster_EM_m,tCaloCluster_EM_phi,tCaloCluster_EM_eta,etaBlocks[1],etaBlocks[2],.4,1000+iEnt);
			calculateDifference3SymConeExample(tCaloCluster_EM_pt,tCaloCluster_EM_e,tCaloCluster_EM_m,tCaloCluster_EM_phi,tCaloCluster_EM_eta,etaBlocks[2],etaBlocks[3],.4,2000+iEnt);
			calculateDifference3SymConeExample(tCaloCluster_EM_pt,tCaloCluster_EM_e,tCaloCluster_EM_m,tCaloCluster_EM_phi,tCaloCluster_EM_eta,etaBlocks[3],etaBlocks[4],.4,3000+iEnt);
			calculateDifference3SymConeExample(tCaloCluster_EM_pt,tCaloCluster_EM_e,tCaloCluster_EM_m,tCaloCluster_EM_phi,tCaloCluster_EM_eta,etaBlocks[4],etaBlocks[5],.4,4000+iEnt);
			calculateDifference3SymConeExample(tCaloCluster_EM_pt,tCaloCluster_EM_e,tCaloCluster_EM_m,tCaloCluster_EM_phi,tCaloCluster_EM_eta,etaBlocks[5],etaBlocks[6],.4,5000+iEnt);
			calculateDifference3SymConeExample(tCaloCluster_EM_pt,tCaloCluster_EM_e,tCaloCluster_EM_m,tCaloCluster_EM_phi,tCaloCluster_EM_eta,etaBlocks[6],etaBlocks[7 ],.4,6000+iEnt);
		}
	}
	for (int iEnt = 0; iEnt < nEntries; iEnt++) {

		//cout<<"after entering for loop"<<endl;
		inTree->GetEntry(iEnt); // Gets the next entry (filling the linked variables)
		//cout<<"heyo1"<<endl;
		if (iEnt%500==0) {
			cout<<"Finished processing "<<iEnt<< " events!!!"<<endl;
		}

		if (isMC) {
			weight=tEventWeight*tPileupWeight;
		}
		averageMuHisto->Fill(taverageMu,weight);
		if (muScale) {
			correctedAverageMuHisto->Fill(tcorrectedAverageMu/1.16,weight);
		} else {
			correctedAverageMuHisto->Fill(tcorrectedAverageMu,weight);
		}
		NPVHisto->Fill(tNPV_n,weight);
		//			cout<< "taverageMu "<<taverageMu<<endl;
		//cout<<"heyo3"<<endl;

		for (int x=0;x<nEtaSteps;x++) {
			//if (x==1) {
				//tempDifference=calculateDifference3Sym(tCaloCluster_EM_pt,tCaloCluster_EM_e,tCaloCluster_EM_m,tCaloCluster_EM_phi,tCaloCluster_EM_eta,etaBlocks[x],etaBlocks[x+1],.4,1,true,Eta1Test,Eta2Test,Phi1Test,Phi2Test);
			//} else {
			tempDifference=calculateDifference3Sym(tCaloCluster_EM_pt,tCaloCluster_EM_e,tCaloCluster_EM_m,tCaloCluster_EM_phi,tCaloCluster_EM_eta,etaBlocks[x],etaBlocks[x+1],.4,4);
			//}
			if (tempDifference <2500000) {
				emptyConesSkippedHistoEM[x]->Fill(0);
				if (tNPV_n<maxNPV&&tcorrectedAverageMu<maxMu&&tcorrectedAverageMu>=minMu&&tNPV_n>=minNPV) {
					if (muScale) {
						R4ConeEM_etaSlicesPos[x]->Fill(tempDifference,tNPV_n,tcorrectedAverageMu/1.16,weight);
					} else {
						R4ConeEM_etaSlicesPos[x]->Fill(tempDifference,tNPV_n,tcorrectedAverageMu,weight);
					}

				} else {
					cout <<"				NPV="<<tNPV_n<<"  Mu="<< tcorrectedAverageMu<<endl;
				}
			} else {
				emptyConesSkippedHistoEM[x]->Fill(1);
			}
			tempDifference=calculateDifference3Sym(tCaloCluster_LC_pt,tCaloCluster_LC_e,tCaloCluster_LC_m,tCaloCluster_LC_phi,tCaloCluster_LC_eta,etaBlocks[x],etaBlocks[x+1],.4,4);
			if (tempDifference <2500000) {
				emptyConesSkippedHistoLC[x]->Fill(0);
				if (tNPV_n<maxNPV&&tcorrectedAverageMu<maxMu&&tcorrectedAverageMu>=minMu&&tNPV_n>=minNPV) {
					if (muScale) {
						R4ConeLC_etaSlicesPos[x]->Fill(tempDifference,tNPV_n,tcorrectedAverageMu/1.16,weight);
					} else {
						R4ConeLC_etaSlicesPos[x]->Fill(tempDifference,tNPV_n,tcorrectedAverageMu,weight);
					}
				} else {
					cout <<"				NPV="<<tNPV_n<<"  Mu="<< tcorrectedAverageMu<<endl;
				}
			} else {
				emptyConesSkippedHistoLC[x]->Fill(1);
			}

		}
		if (doSecondary==true) {
			for (int x=0;x<nEtaStepsSecondary;x++) {
				//if (x==1) {
					//tempDifference=calculateDifference3Sym(tCaloCluster_EM_pt,tCaloCluster_EM_e,tCaloCluster_EM_m,tCaloCluster_EM_phi,tCaloCluster_EM_eta,etaBlocks[x],etaBlocks[x+1],.4,1,true,Eta1Test,Eta2Test,Phi1Test,Phi2Test);
				//} else {
				tempDifference=calculateDifference3Sym(tCaloCluster_EM_pt,tCaloCluster_EM_e,tCaloCluster_EM_m,tCaloCluster_EM_phi,tCaloCluster_EM_eta,etaBlocksSecondary[x],etaBlocksSecondary[x+1],.4,2);
				//}
				if (tempDifference <2500000) {
					if (tNPV_n<maxNPV&&tcorrectedAverageMu<maxMu&&tcorrectedAverageMu>=minMu&&tNPV_n>=minNPV) {
						if (muScale) {
							R4ConeEM_etaSlicesSecondary[x]->Fill(tempDifference,tNPV_n,tcorrectedAverageMu/1.16,weight);
						} else {
							R4ConeEM_etaSlicesSecondary[x]->Fill(tempDifference,tNPV_n,tcorrectedAverageMu,weight);
						}

					} else {
						cout <<"				NPV="<<tNPV_n<<"  Mu="<< tcorrectedAverageMu<<endl;
					}
				}
				tempDifference=calculateDifference3Sym(tCaloCluster_LC_pt,tCaloCluster_LC_e,tCaloCluster_LC_m,tCaloCluster_LC_phi,tCaloCluster_LC_eta,etaBlocksSecondary[x],etaBlocksSecondary[x+1],.4,2);
				if (tempDifference <2500000) {
					if (tNPV_n<maxNPV&&tcorrectedAverageMu<maxMu&&tcorrectedAverageMu>=minMu&&tNPV_n>=minNPV) {
						if (muScale) {
							R4ConeLC_etaSlicesSecondary[x]->Fill(tempDifference,tNPV_n,tcorrectedAverageMu/1.16,weight);
						} else {
							R4ConeLC_etaSlicesSecondary[x]->Fill(tempDifference,tNPV_n,tcorrectedAverageMu,weight);
						}
					} else {
						cout <<"				NPV="<<tNPV_n<<"  Mu="<< tcorrectedAverageMu<<endl;
					}
				}

			}
		}




	}

	TFile *outFile=new TFile((outputFilename).c_str(),"RECREATE");
	for (int x=0;x<nEtaSteps;x++) {
		R4ConeEM_etaSlicesPos[x]->Write();
		R4ConeLC_etaSlicesPos[x]->Write();
		emptyConesSkippedHistoEM[x]->Write();
		emptyConesSkippedHistoLC[x]->Write();

	}
	averageMuHisto->Write();
	correctedAverageMuHisto->Write();
	NPVHisto->Write();
	if (doSecondary==true) {
		for (int x=0;x<nEtaStepsSecondary;x++) {
			R4ConeEM_etaSlicesSecondary[x]->Write();
			R4ConeLC_etaSlicesSecondary[x]->Write();
		}
	}

	outFile->Close();


}